package czajkowski.patryk.visitedplaces.viewmodel.profile

import androidx.lifecycle.*
import czajkowski.patryk.visitedplaces.model.repository.PlacesRepository
import czajkowski.patryk.visitedplaces.model.repository.UserRepository
import czajkowski.patryk.visitedplaces.viewmodel.base.LoggedInViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.time.LocalDate
import czajkowski.patryk.visitedplaces.model.data.place.PlaceModelWrapper
import czajkowski.patryk.visitedplaces.model.data.user.UserModelWrapper


class ProfileViewModel(
    private val userRepository: UserRepository,
    placesRepository: PlacesRepository
) : LoggedInViewModel(placesRepository) {

    val user = MutableLiveData<UserModelWrapper>()
    val placesStatisticViewState = MutableLiveData<ProfileStatisticViewState>()

    init {
        fetchPlaces()
        fetchUser()
    }

    fun setStatistic(places: List<PlaceModelWrapper>) {
        placesStatisticViewState.postValue(
            ProfileStatisticViewState(
                amountOfCountriesVisited = places.filter { it.country != null }.map { it.country }
                    .distinct().size,
                amountOfCitiesVisited = places.filter { it.city != null }.map { it.city }
                    .distinct().size,
                countryNameMostPlaces = places.filter { it.country != null }
                    .groupBy { it.country }.entries.maxBy { it.value.size }?.key ?: "",
                cityNameMostPlaces = places.filter { it.city != null }
                    .groupBy { it.city }.entries.maxBy { it.value.size }?.key ?: "",
                mostPlacesInYear = places.map { it.visitedDate?.take(4) }.groupBy { it }
                    .maxBy { it.value.size }?.key ?: "",
                placesThisYear = places.filter { it.visitedDate?.take(4) == LocalDate.now().year.toString() }.size
            )
        )
    }

    fun setUserLoggedOut() {
        userRepository.getLoggedUserFromDb()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy (
                onNext = {
                    it.isLogged = false
                    userRepository.updateUserOnDb(it)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                            onError = {
                                errorMessage.postValue(it.message)
                            },
                            onComplete = {
                                isLogout.postValue(true)
                            }
                        )
                },
                onError = {
                    errorMessage.postValue(it.message)
                }
            ).addTo(disposables)
    }

    private fun fetchUser() {
        userRepository.getLoggedUserFromDb()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = {
                    errorMessage.postValue(it.message)
                },
                onNext = {
                    user.postValue(it)
                }
            ).addTo(disposables)
    }

}