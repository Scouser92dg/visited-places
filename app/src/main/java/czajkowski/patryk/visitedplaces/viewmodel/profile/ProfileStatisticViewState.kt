package czajkowski.patryk.visitedplaces.viewmodel.profile

data class ProfileStatisticViewState (
    var amountOfCountriesVisited: Int = 0,
    var amountOfCitiesVisited: Int = 0,
    var countryNameMostPlaces: String = "",
    var cityNameMostPlaces: String = "",
    var mostPlacesInYear: String = "",
    var placesThisYear: Int = 0
)