package czajkowski.patryk.visitedplaces.viewmodel.home.map

import android.location.Address
import androidx.lifecycle.MutableLiveData
import czajkowski.patryk.visitedplaces.model.data.place.NewPlaceRequest
import czajkowski.patryk.visitedplaces.model.data.rate.NewRateRequest
import czajkowski.patryk.visitedplaces.model.repository.PlacesRepository
import czajkowski.patryk.visitedplaces.model.repository.RatesRepository
import czajkowski.patryk.visitedplaces.model.repository.UserRepository
import czajkowski.patryk.visitedplaces.viewmodel.base.LoggedInViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers


class MapViewModel(private val userRepository: UserRepository,
                   private val placesRepository: PlacesRepository,
                   private val ratesRepository: RatesRepository)
    : LoggedInViewModel(placesRepository) {
    
    val isRemoveVisible = MutableLiveData<Boolean>()

    fun addPlace(address: Address, placeDescription: String?, visitedDate: String, rate: Double) {
        userRepository.getLoggedUserFromDb()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onError = {
                    errorMessage.postValue(it.message)
                },
                onNext = { user ->
                    val request = NewPlaceRequest(
                        address.latitude,
                        address.longitude,
                        address.countryName,
                        address.locality,
                        visitedDate,
                        placeDescription,
                        user.id
                    )
                    placesRepository.addPlace(request)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                            onError = {
                                errorMessage.postValue(it.message)
                            },
                            onNext = {
                                ratePlace(it.placeId, rate, it.description)
                            },
                            onComplete = {
                                fetchPlaces()
                            }
                        ).addTo(disposables)

                }
            ).addTo(disposables)
    }

    fun removePlace(placeId: Int) {
        placesRepository.removePlace(placeId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onError = {
                    errorMessage.postValue(it.message)
                },
                onComplete = {
                    fetchPlaces()
                    isRemoveVisible.postValue(false)
                }
            ).addTo(disposables)
    }

    private fun ratePlace(placeId: Int, rate: Double, comment: String?) {
        userRepository.getLoggedUserFromDb()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = {
                    errorMessage.postValue(it.message)
                },
                onNext = {
                    ratesRepository.addRate(NewRateRequest(it.id, placeId, rate, comment))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                            onError = {
                                errorMessage.postValue(it.message)
                            },
                            onComplete = {
                                fetchPlaces()
                                isRemoveVisible.postValue(false)
                            }
                        ).addTo(disposables)
                }
            ).addTo(disposables)

    }

}