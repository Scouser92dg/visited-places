package czajkowski.patryk.visitedplaces.viewmodel.places.details

import androidx.lifecycle.MutableLiveData
import czajkowski.patryk.visitedplaces.model.data.rate.NewRateRequest
import czajkowski.patryk.visitedplaces.model.repository.PlacesRepository
import czajkowski.patryk.visitedplaces.model.repository.RatesRepository
import czajkowski.patryk.visitedplaces.model.repository.UserRepository
import czajkowski.patryk.visitedplaces.viewmodel.base.BaseViewModel
import czajkowski.patryk.visitedplaces.model.data.place.PlaceModelWrapper
import czajkowski.patryk.visitedplaces.model.data.rate.RateModelWrapper
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class PlaceViewModel(
    private val placesRepository: PlacesRepository,
    private val ratesRepository: RatesRepository,
    private val userRepository: UserRepository
): BaseViewModel() {

    val errorMessage = MutableLiveData<String>()
    val rates = MutableLiveData<List<RateModelWrapper>>()
    val place = MutableLiveData<PlaceModelWrapper>()
    val isButtonEnabled = MutableLiveData<Boolean>()
    val isGoToPlaces = MutableLiveData<Boolean>()
    val isRatingAvailable = MutableLiveData<Boolean>()

    fun fetchPlace(placeId: Int) {
        placesRepository.getPlace(placeId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onError = {
                    errorMessage.postValue(it.message)
                }, onNext = {
                    place.postValue(it)
                }
            ).addTo(disposables)
    }

    fun fetchRates(placeId: Int) {
        ratesRepository.getRates(placeId)
            .map {
                it.map {
                    var imageUrl: String? = null
                    userRepository.getUserFromApi(it.userId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeBy (
                            onError = {
                                errorMessage.postValue(it.message)
                            },
                            onNext = { user ->
                                imageUrl = user.imageUrl
                            }
                        ).addTo(disposables)
                    RateModelWrapper(it.placeId, it.userId, imageUrl, it.rate, it.comment)
                }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onError = {
                    errorMessage.postValue(it.message)
                }, onNext = { ratesToSet ->
                    rates.postValue(ratesToSet)
                    userRepository.getLoggedUserFromDb()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeBy(
                            onError = {
                                errorMessage.postValue(it.message)
                            }, onNext = {
                                isRatingAvailable.postValue(!ratesToSet.map { it.userId }.contains(it.id))
                            }
                        ).addTo(disposables)
                    ratesRepository.deleteRatesFromDb(placeId)
                        .andThen(Completable.defer {
                            ratesRepository.storeRatesInDb(ratesToSet)
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                            onError = {
                                errorMessage.postValue(it.message)
                            }).addTo(disposables)
                }
            ).addTo(disposables)
    }

    fun ratePlace(rateValue: Float, comment: String?) {
        userRepository.getLoggedUserFromDb()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onError = {
                    errorMessage.postValue(it.message)
                },
                onNext = {
                    ratesRepository.addRate(
                        request = NewRateRequest(
                        userId = it.id,
                        placeId = place.value?.placeId?:0,
                        rate = rateValue.toDouble(),
                        comment = comment)
                    )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeBy(
                            onError = {
                                errorMessage.postValue(it.message)
                                isButtonEnabled.postValue(true)
                            },
                            onComplete = {
                                isButtonEnabled.postValue(false)
                                isGoToPlaces.postValue(true)
                            }
                        ).addTo(disposables)
                }
            ).addTo(disposables)
    }

}