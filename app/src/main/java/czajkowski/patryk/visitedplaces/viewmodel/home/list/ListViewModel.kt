package czajkowski.patryk.visitedplaces.viewmodel.home.list

import czajkowski.patryk.visitedplaces.model.repository.PlacesRepository
import czajkowski.patryk.visitedplaces.viewmodel.base.LoggedInViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers


class ListViewModel(private val placesRepository: PlacesRepository)
    : LoggedInViewModel(placesRepository) {

    fun removePlace(id: Int) {
        placesRepository.removePlace(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onError = {
                    errorMessage.postValue(it.message)
                },
                onComplete = {
                    fetchPlaces()
                }

            )
            .addTo(disposables)
    }

}