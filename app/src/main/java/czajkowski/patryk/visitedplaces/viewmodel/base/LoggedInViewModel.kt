package czajkowski.patryk.visitedplaces.viewmodel.base

import androidx.lifecycle.MutableLiveData
import czajkowski.patryk.visitedplaces.model.repository.PlacesRepository
import czajkowski.patryk.visitedplaces.model.data.place.PlaceModelWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

abstract class LoggedInViewModel(
    private val placesRepository: PlacesRepository
) : BaseViewModel() {

    open val places = MutableLiveData<List<PlaceModelWrapper>>()
    open val errorMessage = MutableLiveData<String>()
    open val isLogout = MutableLiveData<Boolean>()

    init {
        fetchPlaces()
    }

    protected fun fetchPlaces() {
        placesRepository.getUserPlaces()
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            //Drop DB data if we can fetch item fast enough from the API
            //to avoid UI flickers
            .onErrorReturn {
                listOf()
            }.subscribeBy(
                onError = {
                    errorMessage.postValue(it.message)
                    isLogout.postValue(true)
                },
                onNext = { list ->
                    places.postValue(list.sortedBy { it.country })
                }
            ).addTo(disposables)
    }

}