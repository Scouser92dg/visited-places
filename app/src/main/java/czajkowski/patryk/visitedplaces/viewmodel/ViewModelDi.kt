package czajkowski.patryk.visitedplaces.viewmodel

import czajkowski.patryk.visitedplaces.viewmodel.home.list.ListViewModel
import czajkowski.patryk.visitedplaces.viewmodel.home.map.MapViewModel
import czajkowski.patryk.visitedplaces.viewmodel.login.LoginViewModel
import czajkowski.patryk.visitedplaces.viewmodel.places.details.PlaceViewModel
import czajkowski.patryk.visitedplaces.viewmodel.places.list.PlacesViewModel
import czajkowski.patryk.visitedplaces.viewmodel.profile.ProfileViewModel
import org.koin.dsl.module
import org.koin.androidx.viewmodel.dsl.viewModel


val viewModelModule = module {
    viewModel { LoginViewModel(get()) }
    viewModel { MapViewModel(get(),get(), get()) }
    viewModel { ProfileViewModel(get(), get()) }
    viewModel { ListViewModel(get()) }
    viewModel { PlaceViewModel(get(), get(), get()) }
    viewModel { PlacesViewModel(get()) }
}