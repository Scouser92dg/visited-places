package czajkowski.patryk.visitedplaces.viewmodel.login

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.GraphResponse
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import czajkowski.patryk.visitedplaces.model.repository.UserRepository
import czajkowski.patryk.visitedplaces.viewmodel.base.BaseViewModel
import czajkowski.patryk.visitedplaces.model.data.user.UserModelWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject

class LoginViewModel(private val userRepository: UserRepository) : BaseViewModel() {

    companion object {
        const val FACEBOOK_FIELD_ID = "id"
        const val FACEBOOK_FIELD_NAME = "name"
        const val FACEBOOK_REQUEST_PARAMETER_FIELDS = "fields"
        const val FACEBOOK_REQUEST_PARAMETER_FIELDS_VALUE = "id,first_name,name,last_name,email,gender,birthday"
    }

    val errorMessage = MutableLiveData<String>()
    val isGoToHome = MutableLiveData<Boolean>()

    private fun saveUser(user: UserModelWrapper) {
        userRepository.storeUser(user)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onError = {
                    errorMessage.postValue(it.message)
                },
                onComplete = {
                    isGoToHome.postValue(true)
                }
            )
            .addTo(disposables)
    }

    fun loginByFacebook(accessToken: AccessToken) {
        val request =
            GraphRequest.newMeRequest(accessToken) { jsonObj: JSONObject, response: GraphResponse ->
                val user = getUserFromJSON(jsonObj)
                saveUser(user)
            }

        val parameters = Bundle()
        parameters.putString(FACEBOOK_REQUEST_PARAMETER_FIELDS, FACEBOOK_REQUEST_PARAMETER_FIELDS_VALUE)
        request.parameters = parameters
        request.executeAsync()
    }

    fun loginByGoogle(googleSignInClient: GoogleSignInClient, data: Intent, errorMessage: String) {
        val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
        result?.let {
            val signInAccount = googleSignInClient.silentSignIn()
            if (signInAccount.isSuccessful) {
                signInAccount?.result?.let { account ->
                    account.id?.let { id ->
                        saveUser(UserModelWrapper(id, account.displayName, account.photoUrl.toString(), true))
                    }
                }
            } else {
                this.errorMessage.postValue(errorMessage)
            }
        }
    }

    private fun getUserFromJSON(jsonObj: JSONObject): UserModelWrapper {
        lateinit var name: String
        lateinit var id: String

        if (jsonObj.has(FACEBOOK_FIELD_ID)) {
            id = jsonObj.getString(FACEBOOK_FIELD_ID)
        }
        if (jsonObj.has(FACEBOOK_FIELD_NAME)) {
            name = jsonObj.getString(FACEBOOK_FIELD_NAME)
        }

        val imageUrl = "https://graph.facebook.com/$id/picture?return_ssl_resources=1"
        return UserModelWrapper(id, name, imageUrl, true)
    }

}