package czajkowski.patryk.visitedplaces.viewmodel.places.list

import androidx.lifecycle.MutableLiveData
import czajkowski.patryk.visitedplaces.model.repository.PlacesRepository
import czajkowski.patryk.visitedplaces.viewmodel.base.BaseViewModel
import czajkowski.patryk.visitedplaces.model.data.place.PlaceModelWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class PlacesViewModel(
    private val placesRepository: PlacesRepository
) : BaseViewModel() {

    val errorMessage = MutableLiveData<String>()
    val places = MutableLiveData<List<PlaceModelWrapper>>()

    init {
        fetchPlaces()
    }

    private fun fetchPlaces() {
        placesRepository.getOtherUserPlaces()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { list ->
                    places.postValue(list)

                }, onError = {
                    errorMessage.postValue(it.message)
                }
            ).addTo(disposables)
    }

}