package czajkowski.patryk.visitedplaces.util

import czajkowski.patryk.visitedplaces.model.data.place.PlaceModelWrapper

fun PlaceModelWrapper.getDisplayName(): String {
    var placeName = ""
    if (this.city == null && this.country == null) {
        return "${this.latitude}, ${this.longitude}"
    } else {
        val names = mutableListOf<String>()
        this.country?.let {
            names.add(it)
        }
        this.city?.let {
            names.add(it)
        }
        placeName = names.joinToString(separator = ", ")
        return placeName
    }
}