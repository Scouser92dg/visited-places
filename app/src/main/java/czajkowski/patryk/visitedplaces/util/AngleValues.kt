package czajkowski.patryk.visitedplaces.util

class AngleValues {
    companion object {
        const val ROTATION_180 = 180F
        const val ROTATION_0 = 0F
    }
}