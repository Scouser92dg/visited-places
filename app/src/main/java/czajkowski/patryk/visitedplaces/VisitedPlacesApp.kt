package czajkowski.patryk.visitedplaces

import android.app.Application
import czajkowski.patryk.visitedplaces.model.api.serviceModule
import czajkowski.patryk.visitedplaces.model.db.daoModule
import czajkowski.patryk.visitedplaces.model.repository.repositoryModule
import czajkowski.patryk.visitedplaces.viewmodel.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class VisitedPlacesApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@VisitedPlacesApp)
            modules(
                listOf(
                    serviceModule,
                    viewModelModule,
                    repositoryModule,
                    daoModule
                )
            )
        }
    }
}