package czajkowski.patryk.visitedplaces.view.places.list

import android.os.Bundle
import android.view.View
import com.mikepenz.fastadapter.diff.DiffCallback
import czajkowski.patryk.visitedplaces.R
import czajkowski.patryk.visitedplaces.model.data.place.PlaceModelWrapper
import czajkowski.patryk.visitedplaces.util.SimpleAbstractItem
import czajkowski.patryk.visitedplaces.util.getDisplayName
import kotlinx.android.synthetic.main.list_item_user_place.view.*

class PlaceToCommentListItem(
    private val place: PlaceModelWrapper,
    val actionClick: (placeId: Int) -> Unit
) : SimpleAbstractItem() {

    override fun bind(view: View) {
        view.placeItemName.text = place.getDisplayName()
        view.placeItemUserName.text = place.userName

        view.setOnClickListener {
            actionClick(place.placeId)
        }

    }

    override val layoutRes: Int = R.layout.list_item_user_place

    class EventDiffCallback : DiffCallback<PlaceToCommentListItem> {
        override fun areItemsTheSame(
            oldItem: PlaceToCommentListItem,
            newItem: PlaceToCommentListItem
        ): Boolean {
            return true
        }

        override fun areContentsTheSame(
            oldItem: PlaceToCommentListItem,
            newItem: PlaceToCommentListItem
        ): Boolean {
            return oldItem.place.placeId == newItem.place.placeId
        }

        override fun getChangePayload(
            oldItem: PlaceToCommentListItem,
            oldItemPosition: Int,
            newItem: PlaceToCommentListItem,
            newItemPosition: Int
        ): Any? {
            val diff = Bundle()
            if (newItem.place.placeId != oldItem.place.placeId) {
                diff.putInt("placeId", newItem.place.placeId)
            }
            if (diff.size() == 0) {
                return null
            }
            return diff
        }
    }

}