package czajkowski.patryk.visitedplaces.view.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.squareup.picasso.Picasso
import czajkowski.patryk.visitedplaces.R
import czajkowski.patryk.visitedplaces.view.base.LoggedInFragment
import czajkowski.patryk.visitedplaces.model.data.user.UserModelWrapper
import czajkowski.patryk.visitedplaces.viewmodel.profile.ProfileViewModel
import czajkowski.patryk.visitedplaces.util.setVisible
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class ProfileFragment : LoggedInFragment<ProfileViewModel>(), Toolbar.OnMenuItemClickListener {

    override val viewModel: ProfileViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        profileToolbar.inflateMenu(R.menu.menu_profile)
        profileToolbar.setOnMenuItemClickListener(this)

        viewModel.placesStatisticViewState.observe(viewLifecycleOwner, Observer {
            profileCountriesVisitedText.setText(it.amountOfCountriesVisited.toString())
            profileCitiesVisitedText.setText(it.amountOfCitiesVisited.toString())
            profilePlacesCountryText.setText(it.countryNameMostPlaces)
            profilePlacesCityText.setText(it.cityNameMostPlaces)
            profilePlacesCountryLayout.setVisible(it.amountOfCountriesVisited > 0)
            profilePlacesCityLayout.setVisible(it.amountOfCitiesVisited > 0)
            profileMostPlacesYearText.setText(it.mostPlacesInYear)
            profilePlacesThisYearText.setText(it.placesThisYear.toString())
        })

        viewModel.places.observe(viewLifecycleOwner, Observer {
            viewModel.setStatistic(it)
            profilePlacesThisYearLayout.setVisible(it.isNotEmpty())
            profileMostPlacesYearLayout.setVisible(it.isNotEmpty())
        })

        viewModel.user.observe(viewLifecycleOwner, Observer {
            setUserData(it)
        })

    }

    private fun setUserData(user: UserModelWrapper) {
        profileNameLabel.text = user.name
        if (user.imageUrl != null) {
            Picasso.get().load(user.imageUrl).placeholder(R.drawable.ic_profile)
                .into(profileAvatarImageView)
        } else {
            profileAvatarImageView.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.ic_profile
                )
            )
        }
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.profileLogoutMenuItem -> {
                viewModel.setUserLoggedOut()
            }
        }
        return false
    }

    override fun goToLoginFragment() {
        activity?.findNavController(R.id.nav_host_fragment)?.let { navController ->
            if (navController.currentDestination?.id == R.id.profileFragment) {
                navController.navigate(R.id.action_profileFragment_to_loginFragment)
            }
        }
    }

}