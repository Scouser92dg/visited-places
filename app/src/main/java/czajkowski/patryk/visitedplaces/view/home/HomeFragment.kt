package czajkowski.patryk.visitedplaces.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import czajkowski.patryk.visitedplaces.R
import czajkowski.patryk.visitedplaces.view.home.list.ListFragment
import czajkowski.patryk.visitedplaces.view.home.map.MapFragment
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    private var selectedFragmentId = R.id.homeMapMenuItem

    companion object {
        const val BUNDLE_SELECTED_FRAGMENT_ID = "selected_fragment_id"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        savedInstanceState?.getInt(BUNDLE_SELECTED_FRAGMENT_ID)?.let {
            selectedFragmentId = it
        }

        val selectedMenuOption = homeBottomNavigation.menu.findItem(selectedFragmentId)
        setFragmentBySelectedItem(selectedMenuOption)

        homeBottomNavigation.setOnNavigationItemSelectedListener {
            setFragmentBySelectedItem(it)
        }
    }

    private fun setFragmentBySelectedItem(it: MenuItem): Boolean {
        selectedFragmentId = it.itemId
        return when (it.itemId) {
            R.id.homeListMenuItem -> {
                loadFragment(ListFragment.newInstance())
                true
            }
            R.id.homeMapMenuItem -> {
                loadFragment(MapFragment.newInstance())
                true
            }
            else -> false
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(BUNDLE_SELECTED_FRAGMENT_ID, selectedFragmentId)
    }

    private fun loadFragment(fragment: Fragment) {
        activity?.getSupportFragmentManager()
            ?.beginTransaction()
            ?.replace(R.id.homeFragmentContainer, fragment)
            ?.commit()
    }

}