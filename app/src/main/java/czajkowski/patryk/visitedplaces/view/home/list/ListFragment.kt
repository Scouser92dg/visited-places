package czajkowski.patryk.visitedplaces.view.home.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.snackbar.Snackbar
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil
import czajkowski.patryk.visitedplaces.R
import czajkowski.patryk.visitedplaces.viewmodel.home.list.ListViewModel
import czajkowski.patryk.visitedplaces.view.base.LoggedInFragment
import czajkowski.patryk.visitedplaces.model.data.place.PlaceModelWrapper
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class ListFragment : LoggedInFragment<ListViewModel>(), Toolbar.OnMenuItemClickListener {

    private val adapter = FastItemAdapter<PlaceListItem>()
    override val viewModel: ListViewModel by viewModel()

    companion object {

        fun newInstance(): ListFragment {
            return ListFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeListToolbar.inflateMenu(R.menu.menu_home_list)
        homeListToolbar.setOnMenuItemClickListener(this)

        viewModel.places.observe(viewLifecycleOwner, Observer {
            setItemsOnAdapter(it)
        })
        viewModel.errorMessage.observe(viewLifecycleOwner, Observer {
            Snackbar.make(listFragmentLayout, it, Snackbar.LENGTH_LONG).show()
        })

        prepareList()
    }

    private fun prepareList() {
        placesRecycler.adapter = adapter
        val itemDecorator = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        ContextCompat.getDrawable(requireContext(), R.drawable.recycler_divider)?.let {
            itemDecorator.setDrawable(it)
            placesRecycler.addItemDecoration(itemDecorator)
        }
    }

    private fun setItemsOnAdapter(items: List<PlaceModelWrapper>) {
        FastAdapterDiffUtil.set(
            adapter.itemAdapter,
            items.map {
                PlaceListItem(it) { placeId, placeName ->
                    showRemovePlaceDialog(placeId, placeName)
                    true
                }
            },
            PlaceListItem.EventDiffCallback()
        )
    }

    private fun showRemovePlaceDialog(placeId: Int, placeName: String) {
        MaterialDialog(requireContext()).show {
            this.title(R.string.home_list_dialog_remove_title, context.getString(R.string.home_list_dialog_remove_title, placeName))
            this.positiveButton {
                viewModel.removePlace(placeId)
            }
            this.negativeButton(android.R.string.cancel)
        }
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.homeListProfileMenuItem -> {
                activity?.findNavController(R.id.nav_host_fragment)
                    ?.navigate(R.id.action_homeFragment_to_profileFragment)
            }
            R.id.homeListPlacesMenuItem -> {
                activity?.findNavController(R.id.nav_host_fragment)
                    ?.navigate(R.id.action_homeFragment_to_placesFragment)
            }
        }
        return false
    }

    override fun goToLoginFragment() {
        activity?.findNavController(R.id.nav_host_fragment)?.let { navController ->
            if (navController.currentDestination?.id == R.id.homeFragment) {
                navController.navigate(R.id.action_homeFragment_to_profileFragment)
            }
        }
    }

}