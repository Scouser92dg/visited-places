package czajkowski.patryk.visitedplaces.view.places.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil
import czajkowski.patryk.visitedplaces.R
import czajkowski.patryk.visitedplaces.model.data.rate.RateModelWrapper
import czajkowski.patryk.visitedplaces.viewmodel.places.details.PlaceViewModel
import czajkowski.patryk.visitedplaces.util.getDisplayName
import kotlinx.android.synthetic.main.fragment_place.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class PlaceFragment : Fragment() {
    val viewModel: PlaceViewModel by viewModel()
    private val adapter = FastItemAdapter<RateListItem>()

    companion object {
        const val BUNDLE_PLACE_ID = "place_id"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_place, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.getInt(BUNDLE_PLACE_ID)?.let { id ->
            viewModel.fetchRates(id)
            viewModel.fetchPlace(id)
        }

        prepareList()

        viewModel.place.observe(viewLifecycleOwner, Observer {
            placeToolbar.setTitle(it.getDisplayName())
        })

        viewModel.rates.observe(viewLifecycleOwner, Observer {
            setItemsOnAdapter(it)
        })

        viewModel.isButtonEnabled.observe(viewLifecycleOwner, Observer {
            enableDisableButton(it)
        })

        viewModel.isGoToPlaces.observe(viewLifecycleOwner, Observer {
            backToPlaceList()
        })

        viewModel.isRatingAvailable.observe(viewLifecycleOwner, Observer {
            rateSectionGroup.isVisible = it
            if (it) {
            placeCommentEditText.requestFocus()
            }
        })

        placeRatingBar.setOnRatingChangeListener { ratingBar, rating ->
            placeRatingBar.rating = rating
            enableDisableButton(rating > 0)
        }

        placeSaveButton.setOnClickListener {
            var comment: String? = null
            placeCommentEditText.text.toString().let {
                if (it.isNotBlank()) {
                    comment = it
                }
            }
            viewModel.ratePlace(placeRatingBar.rating, comment)
        }
    }

    private fun enableDisableButton(enable: Boolean) {
        placeSaveButton.isEnabled = enable
        placeSaveButton.isVisible = enable
    }

    private fun prepareList() {
        placeRatesRecycler.adapter = adapter
        val itemDecorator = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        ContextCompat.getDrawable(requireContext(), R.drawable.recycler_divider)?.let {
            itemDecorator.setDrawable(it)
            placeRatesRecycler.addItemDecoration(itemDecorator)
        }
    }

    private fun setItemsOnAdapter(items: List<RateModelWrapper>) {
        FastAdapterDiffUtil.set(
            adapter.itemAdapter,
            items.map {
                RateListItem(requireContext(), it)
            },
            RateListItem.EventDiffCallback()
        )
    }

    private fun backToPlaceList() {
        activity?.onBackPressed()
    }

}