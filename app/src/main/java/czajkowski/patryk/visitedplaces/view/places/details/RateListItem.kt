package czajkowski.patryk.visitedplaces.view.places.details

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.mikepenz.fastadapter.diff.DiffCallback
import com.squareup.picasso.Picasso
import czajkowski.patryk.visitedplaces.R
import czajkowski.patryk.visitedplaces.model.data.rate.RateModelWrapper
import czajkowski.patryk.visitedplaces.util.SimpleAbstractItem
import kotlinx.android.synthetic.main.list_item_rate.view.*

class RateListItem (val context: Context, val rate: RateModelWrapper) : SimpleAbstractItem() {

    override fun bind(view: View) {
        view.rateItemRatingBar.rating = rate.rate?.toFloat()?:0.0f
        view.rateItemRatingBar.isEnabled = false
        view.rateItemComment.text = rate.comment
        if (rate.userImage?.isNotBlank() ?: false) {
            Picasso.get().load(rate.userImage).placeholder(R.drawable.ic_profile)
                .into(view.rateItemUserImage)
        } else {
            view.rateItemUserImage.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_profile
                )
            )
        }
    }

    override val layoutRes: Int = R.layout.list_item_rate

    class EventDiffCallback : DiffCallback<RateListItem> {
        override fun areItemsTheSame(oldItem: RateListItem, newItem: RateListItem): Boolean {
            return true
        }

        override fun areContentsTheSame(oldItem: RateListItem, newItem: RateListItem): Boolean {
            return oldItem.rate.userId == newItem.rate.userId
        }

        override fun getChangePayload(
            oldItem: RateListItem,
            oldItemPosition: Int,
            newItem: RateListItem,
            newItemPosition: Int
        ): Any? {
            val diff = Bundle()
            if (newItem.rate.userId != oldItem.rate.userId) {
                diff.putString("userId", newItem.rate.userId)
            }
            if (diff.size() == 0) {
                return null
            }
            return diff
        }
    }

}