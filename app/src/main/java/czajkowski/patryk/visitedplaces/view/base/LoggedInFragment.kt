package czajkowski.patryk.visitedplaces.view.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import czajkowski.patryk.visitedplaces.viewmodel.base.LoggedInViewModel


abstract class LoggedInFragment<out VIEW_MODEL : LoggedInViewModel> : Fragment()  {
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var googleSignInOptions: GoogleSignInOptions
    protected abstract val viewModel: VIEW_MODEL

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(requireActivity(), googleSignInOptions)

        viewModel.isLogout.observe(viewLifecycleOwner, Observer {
            logoutFromFacebook()
            logoutFromGoogle()
            goToLoginFragment()
        })
    }

    protected fun logoutFromGoogle() {
        googleSignInClient.signOut()
    }

    protected fun logoutFromFacebook() {
        LoginManager.getInstance().logOut()
    }

    abstract fun goToLoginFragment()

}