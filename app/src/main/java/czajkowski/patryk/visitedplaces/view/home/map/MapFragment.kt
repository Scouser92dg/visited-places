package czajkowski.patryk.visitedplaces.view.home.map

import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.afollestad.date.dayOfMonth
import com.afollestad.date.month
import com.afollestad.date.year
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.datetime.datePicker
import com.afollestad.materialdialogs.input.getInputField
import com.afollestad.materialdialogs.input.input
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import czajkowski.patryk.visitedplaces.R
import czajkowski.patryk.visitedplaces.viewmodel.home.map.MapViewModel
import czajkowski.patryk.visitedplaces.view.base.LoggedInFragment
import czajkowski.patryk.visitedplaces.model.data.place.PlaceModelWrapper
import kotlinx.android.synthetic.main.fragment_map.*
import me.zhanghai.android.materialratingbar.MaterialRatingBar
import java.time.LocalDate
import java.util.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MapFragment : LoggedInFragment<MapViewModel>(), OnMapReadyCallback,
    Toolbar.OnMenuItemClickListener {

    override val viewModel: MapViewModel by viewModel()
    private lateinit var map: GoogleMap
    private var selectedPlace: Marker? = null

    companion object {
        fun newInstance(): MapFragment {
            return MapFragment()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        mapView.getMapAsync(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeMapToolbar.inflateMenu(R.menu.menu_home_map)
        homeMapToolbar.setOnMenuItemClickListener(this)

        viewModel.places.observe(viewLifecycleOwner, Observer {
            setItemsOnMap(it)
        })
        viewModel.errorMessage.observe(viewLifecycleOwner, Observer {
            Snackbar.make(mapFragmentLayout, it, Snackbar.LENGTH_LONG).show()
        })
        viewModel.isRemoveVisible.observe(viewLifecycleOwner, Observer { isVisible ->
            homeMapToolbar.menu?.let {
                it.findItem(R.id.homeMapRemoveMenuItem).isVisible = isVisible
            }
        })
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap?.let {
            map = it
            map.setOnMapLongClickListener {
                showAddPlaceDateDialog(it.latitude, it.longitude)
            }
            map.setOnMarkerClickListener {
                selectPlace(it)
                false
            }
            map.setOnInfoWindowCloseListener {
                deselectPlace()
            }
            map.setInfoWindowAdapter(MapInfoWindowAdapter(requireContext()))
            map.moveCamera(CameraUpdateFactory.newLatLng(LatLng(0.0, 0.0)))
        }
    }

    private fun setItemsOnMap(items: List<PlaceModelWrapper>) {
        map.clear()
        items.forEach {
            val placeOnMap = map.addMarker(
                MarkerOptions()
                    .position(LatLng(it.latitude, it.longitude))
                    .title(it.description)
                    .snippet(it.rate.toString())
            )
            placeOnMap.tag = it.placeId
        }
    }

    private fun showAddPlaceDateDialog(latitude: Double, longitude: Double) {
        MaterialDialog(requireContext()).show {
            this.title(R.string.dialog_title_type_visited_date)
            this.datePicker(dateCallback = { dialog, date ->
                val visitedDate =
                    LocalDate.of(date.year, date.month + 1, date.dayOfMonth).toString()
                showAddPlaceDescriptionDialog(latitude, longitude, visitedDate)
            }, maxDate = Calendar.getInstance())
            this.negativeButton(android.R.string.cancel)
        }
    }

    private fun showAddPlaceDescriptionDialog(
        latitude: Double,
        longitude: Double,
        visitedDate: String
    ) {
        MaterialDialog(requireContext()).show {
            this.title(R.string.dialog_title_type_place_description)
            this.input(allowEmpty = true)
            this.positiveButton {
                val text = it.getInputField().text.toString()
                var placeDescription: String? = null
                if (text.isNotEmpty()) {
                    placeDescription = text
                }
                showAddPlaceRateDialog(latitude, longitude, visitedDate, placeDescription)
            }
            this.negativeButton(android.R.string.cancel)
        }
    }

    private fun showAddPlaceRateDialog(
        latitude: Double,
        longitude: Double,
        visitedDate: String,
        placeDescription: String?
    ) {
        MaterialDialog(requireContext()).show {
            this.title(R.string.dialog_title_rate)
            val view = customView(R.layout.dialog_rate)
            this.positiveButton {
                val rate = view.findViewById<MaterialRatingBar>(R.id.dialogRatingBar).rating
                val address = getAddressData(latitude, longitude)
                viewModel.addPlace(address, placeDescription, visitedDate, rate.toDouble())
            }
            this.negativeButton(android.R.string.cancel)
        }
    }

    private fun getAddressData(latitude: Double, longitude: Double): Address {
        try {
            val geo = Geocoder(requireContext(), Locale.getDefault())
            val addresses: List<Address> = geo.getFromLocation(latitude, longitude, 1)
            if (addresses.isNotEmpty()) {
                return addresses[0]
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Snackbar.make(mapFragmentLayout, e.message.toString(), Snackbar.LENGTH_LONG).show()
        }
        return Address(
            Locale.UK
        ).also {
            it.latitude = latitude
            it.longitude = longitude
        }
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.homeMapProfileMenuItem -> {
                goToProfileFragment()
            }
            R.id.homeMapRemoveMenuItem -> {
                selectedPlace?.let {
                    it.tag?.let {
                        viewModel.removePlace(it as Int)
                    }
                }
            }
        }
        return false
    }

    private fun selectPlace(marker: Marker) {
        selectedPlace = marker
        viewModel.isRemoveVisible.postValue(true)
    }

    private fun deselectPlace() {
        selectedPlace = null
        viewModel.isRemoveVisible.postValue(false)
    }

    private fun goToProfileFragment() {
        activity?.findNavController(R.id.nav_host_fragment)?.let { navController ->
            if (navController.currentDestination?.id == R.id.homeFragment) {
                navController.navigate(R.id.action_homeFragment_to_profileFragment)
            }
        }
    }

    override fun goToLoginFragment() {
        activity?.findNavController(R.id.nav_host_fragment)?.let { navController ->
            if (navController.currentDestination?.id == R.id.homeFragment) {
                navController.navigate(R.id.action_homeFragment_to_profileFragment)
            }
        }
    }

}