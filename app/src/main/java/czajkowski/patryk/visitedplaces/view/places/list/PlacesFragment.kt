package czajkowski.patryk.visitedplaces.view.places.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil
import czajkowski.patryk.visitedplaces.R
import czajkowski.patryk.visitedplaces.model.data.place.PlaceModelWrapper
import czajkowski.patryk.visitedplaces.viewmodel.places.list.PlacesViewModel
import czajkowski.patryk.visitedplaces.view.places.details.PlaceFragment
import kotlinx.android.synthetic.main.fragment_places.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class PlacesFragment: Fragment() {
    val viewModel: PlacesViewModel by viewModel()
    private val adapter = FastItemAdapter<PlaceToCommentListItem>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_places, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.places.observe(viewLifecycleOwner, Observer {
            setItemsOnAdapter(it)
        })

        placesToolbar.setTitle(R.string.places_other_users_places_label)

        prepareList()
    }

    private fun prepareList() {
        usersRecycler.adapter = adapter
        val itemDecorator = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        ContextCompat.getDrawable(requireContext(), R.drawable.recycler_divider)?.let {
            itemDecorator.setDrawable(it)
            usersRecycler.addItemDecoration(itemDecorator)
        }
    }

    private fun setItemsOnAdapter(items: List<PlaceModelWrapper>) {
        FastAdapterDiffUtil.set(
            adapter.itemAdapter,
            items.map {
                PlaceToCommentListItem(it) { placeId ->
                    goToPlaceDetails(placeId)
                }
            },
            PlaceToCommentListItem.EventDiffCallback()
        )
    }

    private fun goToPlaceDetails(placeId: Int) {
        activity?.findNavController(R.id.nav_host_fragment)?.let { navController ->
            if (navController.currentDestination?.id == R.id.placesFragment) {
                val bundle = Bundle()
                bundle.putInt(PlaceFragment.BUNDLE_PLACE_ID, placeId)
                navController.navigate(R.id.action_placesFragment_to_placeFragment, bundle)
            }
        }
    }

}