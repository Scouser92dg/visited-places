package czajkowski.patryk.visitedplaces.view.home.list

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import com.mikepenz.fastadapter.diff.DiffCallback
import czajkowski.patryk.visitedplaces.model.data.place.PlaceModelWrapper
import czajkowski.patryk.visitedplaces.util.AngleValues
import czajkowski.patryk.visitedplaces.util.SimpleAbstractItem
import czajkowski.patryk.visitedplaces.util.getDisplayName
import czajkowski.patryk.visitedplaces.util.setVisible
import kotlinx.android.synthetic.main.list_item_place.view.*

class PlaceListItem (val place: PlaceModelWrapper, val actionRemoveClick: (id: Int, name: String) -> Boolean) : SimpleAbstractItem() {

    override fun bind(view: View) {
        view.placeItemName.text = place.getDisplayName()
        view.placeItemDescription.text = place.description
        view.placeItemVisitedDate.text = place.visitedDate
        view.placeItemExpandIcon.setOnClickListener {
            view.placeItemExpandGroup.setVisible(!view.placeItemExpandGroup.isVisible)
            view.placeItemExpandIcon.rotationX = when (view.placeItemExpandGroup.isVisible) {
                false -> AngleValues.ROTATION_0
                true -> AngleValues.ROTATION_180
            }
        }
        view.placeItemRemoveIcon.setOnClickListener {
            actionRemoveClick(place.placeId, place.getDisplayName())
        }
    }

    override val layoutRes: Int = czajkowski.patryk.visitedplaces.R.layout.list_item_place

    class EventDiffCallback : DiffCallback<PlaceListItem> {
        override fun areItemsTheSame(oldItem: PlaceListItem, newItem: PlaceListItem): Boolean {
            return true
        }

        override fun areContentsTheSame(oldItem: PlaceListItem, newItem: PlaceListItem): Boolean {
            return oldItem.place.latitude == newItem.place.latitude &&
                    oldItem.place.longitude == newItem.place.longitude
        }

        override fun getChangePayload(
            oldItem: PlaceListItem,
            oldItemPosition: Int,
            newItem: PlaceListItem,
            newItemPosition: Int
        ): Any? {
            val diff = Bundle()
            if (newItem.place.latitude != oldItem.place.latitude) {
                diff.putDouble("latitude", newItem.place.latitude)
            }
            if (newItem.place.longitude != oldItem.place.longitude) {
                diff.putDouble("longitude", newItem.place.longitude)
            }
            if (diff.size() == 0) {
                return null
            }
            return diff
        }
    }
}