package czajkowski.patryk.visitedplaces.view.home.map

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import czajkowski.patryk.visitedplaces.R
import kotlinx.android.synthetic.main.view_map_window_info.view.*


class MapInfoWindowAdapter(
    private val context: Context
) : GoogleMap.InfoWindowAdapter {

    override fun getInfoWindow(marker: Marker): View? {
        return null
    }

    override fun getInfoContents(marker: Marker): View? {
        val view =
            (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                R.layout.view_map_window_info,
                null
            )

        view.windowInfoDescription.text = marker.title
        marker.snippet?.let { snippet ->
            view.windowInfoRatingBar.rating = snippet.toFloat()
        }
        return view
    }

}