package czajkowski.patryk.visitedplaces.view.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.facebook.*
import com.facebook.AccessToken
import com.facebook.FacebookSdk.getApplicationContext
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.snackbar.Snackbar
import czajkowski.patryk.visitedplaces.R
import czajkowski.patryk.visitedplaces.viewmodel.login.LoginViewModel
import kotlinx.android.synthetic.main.fragment_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class LoginFragment : Fragment() {

    private val viewModel: LoginViewModel by viewModel()
    lateinit var callbackManager: CallbackManager
    private lateinit var googleSignInClient: GoogleSignInClient

    companion object {
        const val GOOGLE_SIGN_IN = 1
        const val FACEBOOK_SIGN_IN = 64206
        const val FACEBOOK_PERMISSIONS = "email, public_profile"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initFacebookAuth()
        initGoogleAuth()

        loginGoogleButton.setOnClickListener {
            startActivityForResult(googleSignInClient.signInIntent, GOOGLE_SIGN_IN)
        }

        viewModel.errorMessage.observe(viewLifecycleOwner, Observer {
            Snackbar.make(loginFragmentLayout, it, Snackbar.LENGTH_LONG).show()
        })

        viewModel.isGoToHome.observe(viewLifecycleOwner, Observer { readyToGoHome ->
            if (readyToGoHome) goToHomeFragment()
        })
    }

    private fun initGoogleAuth() {
        val googleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
        googleSignInClient = GoogleSignIn.getClient(requireActivity(), googleSignInOptions)
    }

    private fun initFacebookAuth() {
        FacebookSdk.sdkInitialize(getApplicationContext())
        callbackManager = CallbackManager.Factory.create()
        loginFacebookButton.setFragment(this)
        loginFacebookButton.setPermissions(listOf(FACEBOOK_PERMISSIONS))
        loginFacebookButton.registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    viewModel.loginByFacebook(loginResult.accessToken)
                }

                override fun onCancel() {
                    Snackbar.make(
                        loginFragmentLayout,
                        requireContext().getString(R.string.login_facebook_login_canceled),
                        Snackbar.LENGTH_LONG
                    ).show()
                }

                override fun onError(exception: FacebookException) {
                    Snackbar.make(
                        loginFragmentLayout,
                        requireContext().getString(R.string.login_facebook_login_error),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            })
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        when (requestCode) {
            GOOGLE_SIGN_IN -> {
                val googleErrorMessage = getString(R.string.login_google_login_error)
                data?.let {
                    viewModel.loginByGoogle(googleSignInClient, it, googleErrorMessage)
                }
            }
            FACEBOOK_SIGN_IN -> {
                callbackManager.onActivityResult(requestCode, resultCode, data)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (AccessToken.getCurrentAccessToken() != null || GoogleSignIn.getLastSignedInAccount(context) != null) {
            goToHomeFragment()
        }
    }

    private fun goToHomeFragment() {
        activity?.findNavController(R.id.nav_host_fragment)?.let { navController ->
            if (navController.currentDestination?.id == R.id.loginFragment) {
                navController.navigate(R.id.action_loginFragment_to_homeFragment)
            }
        }
    }

}