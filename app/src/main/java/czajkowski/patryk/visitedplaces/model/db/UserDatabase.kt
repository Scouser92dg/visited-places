package czajkowski.patryk.visitedplaces.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import czajkowski.patryk.visitedplaces.model.data.user.UserLocal

@Database(entities = [UserLocal::class], version = UserDatabase.VERSION)
abstract class UserDatabase : RoomDatabase() {
    companion object {
        const val DB_NAME = "user.db"
        const val VERSION = 11

        @Volatile
        private var INSTANCE: UserDatabase? = null

        fun getInstance(context: Context): UserDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: bindDatabase(context).also { INSTANCE = it }
            }

        private fun bindDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, UserDatabase::class.java, DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }

    abstract fun userDao(): UserDao
}