package czajkowski.patryk.visitedplaces.model.repository

import android.util.Log
import czajkowski.patryk.visitedplaces.model.api.VisitedPlacesService
import czajkowski.patryk.visitedplaces.model.db.RatesDao
import czajkowski.patryk.visitedplaces.model.data.rate.NewRateRequest
import czajkowski.patryk.visitedplaces.model.data.rate.RateLocal
import czajkowski.patryk.visitedplaces.model.data.rate.RateModelWrapper
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class RatesRepository (
    private val visitedPlacesService: VisitedPlacesService,
    private val ratesDao: RatesDao
) {

    companion object {
        const val CANNOT_VOTE_PLACE_TWO_TIME_ERROR = "You cannot vote on the same place 2 times"
    }

    fun getRates(placeId: Int): Observable<List<RateModelWrapper>> {
        return Observable.concatArray(
            getRatesFromDb(placeId),
            getRatesFromApi(placeId)
        )
    }

    private fun getRatesFromDb(placeId: Int): Observable<List<RateModelWrapper>> {
        return ratesDao.selectRatesByPlaceId(placeId)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .toObservable()
            .filter { it.isNotEmpty() }
            .map { it.map { RateModelWrapper(it) } }
            .doOnNext {
                Log.d("RatesRepository", "Dispatching ${it.size} rates from DB...")
            }
    }

    private fun getRatesFromApi(placeId: Int): Observable<List<RateModelWrapper>> {
        return visitedPlacesService.getRates()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .map { it.data }
            .map { it.filter { it.placeId == placeId } }
            .map { it.map { RateModelWrapper(it) } }
            .toObservable()
    }

    fun deleteRatesFromDb(placeId: Int): Completable {
        return Completable.fromCallable { ratesDao.deleteRates(placeId) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Completable.error(it) }
            .doOnComplete {
                Log.d("RatesRepository", "All rates removed")
            }
    }

    fun storeRatesInDb(rates: List<RateModelWrapper>): Completable {
        return Completable.fromCallable { ratesDao.insertAllRates(rates.map { RateLocal(it) }) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Completable.error(it) }
            .doOnComplete {
                Log.d("RatesRepository", "Inserted ${rates.size} rates in DB...")
            }
    }

    fun addRate(request: NewRateRequest): Completable {
        return visitedPlacesService.getRates()
            .map { it.data }
            .flatMapCompletable {
                val rate = it.filter { it.placeId == request.placeId && it.userId == request.userId }.firstOrNull()
                if (rate == null) {
                    visitedPlacesService.addRate(request)
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.io())
                        .onErrorResumeNext { Completable.error(it) }
                        .doOnComplete {
                            Log.d(
                                "RatesRepository",
                                "Added rate ${request.rate}, ${request.comment}"
                            )
                        }
                } else {
                    Completable.error(Error(CANNOT_VOTE_PLACE_TWO_TIME_ERROR))
                }
            }
    }

}