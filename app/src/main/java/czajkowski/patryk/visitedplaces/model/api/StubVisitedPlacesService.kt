package czajkowski.patryk.visitedplaces.model.api

import czajkowski.patryk.visitedplaces.model.data.place.*
import czajkowski.patryk.visitedplaces.model.data.rate.NewRateRequest
import czajkowski.patryk.visitedplaces.model.data.rate.RateResponse
import czajkowski.patryk.visitedplaces.model.data.user.NewUserRequest
import czajkowski.patryk.visitedplaces.model.data.user.UserRemote
import czajkowski.patryk.visitedplaces.model.data.user.UserResponse
import io.reactivex.Completable
import io.reactivex.Single

class StubVisitedPlacesService: VisitedPlacesService {

    override fun getPlaces(): Single<PlaceResponse> {
        val places = listOf(
            PlaceRemote(1, "1", 50.0,50.0, "Poland", "Dąbrowa Górnicza", "Pogoria 4", ""),
            PlaceRemote(2, "1",40.0, 40.0, "France", "Paris", "Eiffel Tower", "")
        )
        return Single.just(PlaceResponse(places))
    }

    override fun getPlace(id: Int): Single<PlaceRemote> {
        return Single.just(PlaceRemote(1,"12312",1.0,2.0,"zxczxc","czxxzc","zxczx","czxc"))
    }

    override fun addPlace(request: NewPlaceRequest): Single<PlaceRemote> {
        return Single.just(PlaceRemote(1,"12312",1.0,2.0,"zxczxc","czxxzc","zxczx","czxc"))
    }

    override fun deletePlace(id: Int): Completable {
        return Completable.complete()
    }

    override fun editPlace(id: Int, request: EditPlaceRequest): Completable {
        return Completable.complete()
    }

    override fun getRates(): Single<RateResponse> {
        return Single.just(RateResponse(listOf()))
    }

    override fun addRate(request: NewRateRequest): Completable {
        return Completable.complete()
    }

    override fun getUsers(): Single<UserResponse> {
        return Single.just(UserResponse(listOf(UserRemote("123123123", "user user", "image.jpg"))))
    }

    override fun addUser(request: NewUserRequest): Completable {
        return Completable.complete()
    }
}