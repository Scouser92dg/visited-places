package czajkowski.patryk.visitedplaces.model.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import czajkowski.patryk.visitedplaces.model.data.user.UserLocal
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: UserLocal): Completable

    @Query("DELETE FROM UserLocal where userId = :userId")
    fun deleteUser(userId: String): Completable

    @Query("SELECT * from UserLocal where userId = :userId LIMIT 1")
    fun selectUser(userId: String): Single<UserLocal?>

    @Query("SELECT * from UserLocal")
    fun selectAllUsers(): Single<List<UserLocal>>
}