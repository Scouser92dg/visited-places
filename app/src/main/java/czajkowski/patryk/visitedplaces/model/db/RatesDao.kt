package czajkowski.patryk.visitedplaces.model.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import czajkowski.patryk.visitedplaces.model.data.rate.RateLocal
import io.reactivex.Single

@Dao
interface RatesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRate(rate: RateLocal)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllRates(rates: List<RateLocal>)

    @Query("DELETE FROM RateLocal WHERE placeId = :placeId AND userId = :userId")
    fun deleteRate(placeId: Int, userId: String)

    @Query("DELETE FROM RateLocal WHERE placeId = :placeId")
    fun deleteRates(placeId: Int)

    @Query("DELETE FROM RateLocal")
    fun deleteAllRates()

    @Query("SELECT * from RateLocal where placeId = :placeId")
    fun selectRatesByPlaceId(placeId: Int): Single<List<RateLocal>>

}