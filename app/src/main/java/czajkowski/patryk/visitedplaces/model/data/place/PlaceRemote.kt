package czajkowski.patryk.visitedplaces.model.data.place

data class PlaceRemote (
    var id: Int,
    var userId: String,
    var longitude: Double,
    var latitude: Double,
    var country: String?,
    var city: String?,
    var description: String?,
    var visitedDate: String?
)