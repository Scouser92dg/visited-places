package czajkowski.patryk.visitedplaces.model.data.place

import czajkowski.patryk.visitedplaces.model.data.place.PlaceLocal
import czajkowski.patryk.visitedplaces.model.data.place.PlaceRemote

data class PlaceModelWrapper (
    var placeId: Int,
    var userId: String,
    var longitude: Double,
    var latitude: Double,
    var country: String? = null,
    var city: String? = null,
    var description: String? = null,
    var visitedDate: String? = null,
    var userName: String? = null,
    var rate: Double? = null
) {
    constructor(place: PlaceLocal): this(place.id, place.userId, place.longitude, place.latitude, place.country, place.city, place.description, place.visitedDate)
    constructor(place: PlaceRemote, rate: Double ? = null): this(place.id, place.userId, place.longitude, place.latitude, place.country, place.city, place.description, place.visitedDate, null, rate)
}