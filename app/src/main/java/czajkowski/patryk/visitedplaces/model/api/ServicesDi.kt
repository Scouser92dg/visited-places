package czajkowski.patryk.visitedplaces.model.api

import czajkowski.patryk.visitedplaces.model.api.factory.GsonFactory
import czajkowski.patryk.visitedplaces.model.api.factory.OkHttpClientFactory
import czajkowski.patryk.visitedplaces.model.api.factory.RetrofitFactory
import org.koin.dsl.module
import retrofit2.Retrofit

val serviceModule = module {

    single {
        GsonFactory.createGson()
    }

    single {
        OkHttpClientFactory.createOkHttpClient()
    }

    single {
        RetrofitFactory.createRetrofit(get(), get())
    }

    single<VisitedPlacesService> { get<Retrofit>().create(VisitedPlacesService::class.java) }
//    single<VisitedPlacesService> { StubVisitedPlacesService() }

}
