package czajkowski.patryk.visitedplaces.model.data.rate

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class RateLocal (
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id") var id: Int = 0,
    @SerializedName("placeId") var placeId: Int,
    @SerializedName("userId") var userId: String,
    @SerializedName("userImage") var userImage: String?,
    @SerializedName("rate") var rate: Double?,
    @SerializedName("comment") var comment: String
) {
    constructor(rate: RateModelWrapper): this(placeId = rate.placeId, userId = rate.userId, userImage = rate.userImage, rate = rate.rate, comment = rate.comment)
}