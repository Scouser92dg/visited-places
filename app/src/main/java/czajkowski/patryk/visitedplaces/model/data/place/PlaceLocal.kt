package czajkowski.patryk.visitedplaces.model.data.place

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class PlaceLocal (
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id") var id: Int,
    @SerializedName("userId") var userId: String,
    @SerializedName("longitude") var longitude: Double,
    @SerializedName("latitude") var latitude: Double,
    @SerializedName("country") var country: String?,
    @SerializedName("city") var city: String?,
    @SerializedName("description") var description: String?,
    @SerializedName("visitedDate") var visitedDate: String?
)
{
    constructor(place: PlaceModelWrapper): this(
        id = place.placeId,
        userId = place.userId,
        longitude = place.longitude,
        latitude = place.latitude,
        country = place.country,
        city = place.city,
        description = place.description,
        visitedDate = place.visitedDate
    )
}