package czajkowski.patryk.visitedplaces.model.data.user

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class UserLocal (
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id") val id: Int = 0,
    @SerializedName("isLogged") val isLogged: Boolean,
    @SerializedName("userId") val userId: String,
    @SerializedName("name") val name: String?,
    @SerializedName("imageUrl") val imageUrl: String?
)