package czajkowski.patryk.visitedplaces.model.data.rate

class RateResponse (
    val data: List<RateRemote>
)