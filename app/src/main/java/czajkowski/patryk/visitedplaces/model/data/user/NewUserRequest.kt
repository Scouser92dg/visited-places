package czajkowski.patryk.visitedplaces.model.data.user

class NewUserRequest (
    val userId: String,
    val name: String?,
    val imageUrl: String?
)