package czajkowski.patryk.visitedplaces.model.repository

import android.util.Log
import czajkowski.patryk.visitedplaces.model.api.VisitedPlacesService
import czajkowski.patryk.visitedplaces.model.db.PlaceDao
import czajkowski.patryk.visitedplaces.model.data.place.EditPlaceRequest
import czajkowski.patryk.visitedplaces.model.data.place.NewPlaceRequest
import czajkowski.patryk.visitedplaces.model.data.place.PlaceLocal
import czajkowski.patryk.visitedplaces.model.data.place.PlaceModelWrapper
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class PlacesRepository(
    private val visitedPlacesService: VisitedPlacesService,
    private val userRepository: UserRepository,
    private val ratesRepository: RatesRepository,
    private val placesDao: PlaceDao
) {

    fun getPlace(placeId: Int): Observable<PlaceModelWrapper> {
        return Observable.concatArray(
            getPlaceFromDb(placeId),
            getPlaceFromApi(placeId)
        )
    }

    private fun getPlaceFromDb(placeId: Int): Observable<PlaceModelWrapper> {
        return placesDao.selectPlaceById(placeId)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext (Maybe.empty())
            .toObservable()
            .map { PlaceModelWrapper(it) }
            .doOnNext {
                Log.d("PlacesRepository", "Dispatching ${it.placeId} place from DB...")
            }
    }

    private fun getPlaceFromApi(placeId: Int): Observable<PlaceModelWrapper> {
        return visitedPlacesService.getPlace(placeId)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .toObservable()
            .map { PlaceModelWrapper(it) }
            .doOnNext {
                Log.d("PlacesRepository", "Dispatching ${it.placeId} place from API...")
            }
    }

    fun getOtherUserPlaces(): Observable<List<PlaceModelWrapper>> {
        return userRepository.getLoggedUserFromDb()
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .flatMap { user ->
                Observable.concatArray(
                    getOtherUsersPlacesFromDb(user.id),
                    getOtherUserPlacesFromApi(user.id)
                ).map { places ->
                    synchronizePlaces(places = places)
                    places.flatMap { place ->
                        userRepository.getUserFromApi(place.userId)
                            .observeOn(Schedulers.io())
                            .subscribeOn(Schedulers.io())
                            .map {
                                PlaceModelWrapper(
                                    place.placeId,
                                    place.userId,
                                    place.longitude,
                                    place.latitude,
                                    place.country,
                                    place.city,
                                    place.description,
                                    place.visitedDate,
                                    it.name
                                )
                            }.blockingIterable()
                    }
                }
            }
    }

    fun getUserPlaces(): Observable<List<PlaceModelWrapper>> {
        return userRepository.getLoggedUserFromDb()
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .flatMap { user ->
                Observable.concatArray(
                    getUserPlacesFromDb(user.id),
                    getUserPlacesFromApi(user.id)
                ).map { places ->
                    synchronizePlaces(user.id, places)
                    places.flatMap { place ->
                        ratesRepository.getRates(place.placeId)
                            .observeOn(Schedulers.io())
                            .subscribeOn(Schedulers.io())
                            .map {
                                val rate = it.map { it.rate?:0.0 }.toDoubleArray().average()
                                PlaceModelWrapper(
                                    place.placeId,
                                    place.userId,
                                    place.longitude,
                                    place.latitude,
                                    place.country,
                                    place.city,
                                    place.description,
                                    place.visitedDate,
                                    place.userName,
                                    rate
                                )
                            }.blockingIterable()
                    }
                }
            }
    }

    private fun getOtherUsersPlacesFromDb(userId: String): Observable<List<PlaceModelWrapper>> {
        return placesDao.selectAllPlaces()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .toObservable()
            .filter { it.isNotEmpty() }
            .map { it.filter { it.userId != userId } }
            .map { it.map { PlaceModelWrapper(it) } }
            .doOnNext {
                Log.d("PlacesRepository", "Dispatching ${it.size} places from DB...")
            }
    }

    private fun getOtherUserPlacesFromApi(userId: String): Observable<List<PlaceModelWrapper>> {
        return visitedPlacesService.getPlaces()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .map { it.data }
            .map { it.filter { it.userId != userId } }
            .map { it.map { PlaceModelWrapper(it) } }
            .toObservable()
    }

    private fun getUserPlacesFromDb(userId: String): Observable<List<PlaceModelWrapper>> {
        return placesDao.selectAllPlaces(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .toObservable()
            .filter { it.isNotEmpty() }
            .map { it.map { PlaceModelWrapper(it) } }
            .doOnNext {
                Log.d("PlacesRepository", "Dispatching ${it.size} places from DB...")
            }
    }

    private fun getUserPlacesFromApi(userId: String): Observable<List<PlaceModelWrapper>> {
        return visitedPlacesService.getPlaces()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .map { it.data }
            .map { it.filter { it.userId == userId } }
            .map { it.map { PlaceModelWrapper(it) } }
            .toObservable()
    }

    private fun synchronizePlaces(userId: String? = null, places: List<PlaceModelWrapper>): Completable {
        return if (userId == null) {
            deletePlacesFromDb()
        } else {
            deletePlacesFromDb(userId)
        }.andThen(Completable.defer { storePlacesInDb(places) })
    }

    private fun deletePlacesFromDb(): Completable {
        return Completable.fromCallable { placesDao.deleteAllPlaces() }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Completable.error(it) }
            .doOnComplete {
                Log.d("PlacesRepository", "All places removed")
            }
    }

    private fun deletePlacesFromDb(userId: String): Completable {
        return Completable.fromCallable { placesDao.deletaUserPlaces(userId) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Completable.error(it) }
            .doOnComplete {
                Log.d("PlacesRepository", "All places for user $userId removed")
            }
    }

    private fun storePlacesInDb(places: List<PlaceModelWrapper>): Completable {
        return Completable.fromCallable { placesDao.insertAllPlaces(places.map { PlaceLocal(it) }) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Completable.error(it) }
            .doOnComplete {
                Log.d("PlacesRepository", "Inserted ${places.size} places in DB...")
            }
    }

    fun addPlace(request: NewPlaceRequest): Observable<PlaceModelWrapper> {
        return visitedPlacesService.addPlace(request)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .toObservable()
            .map { PlaceModelWrapper(it) }
            .doOnNext {
                Log.d("PlacesRepository", "Added place ${request.latitude}, ${request.longitude}")
            }
    }

    fun removePlace(placeId: Int): Completable {
        return removePlaceFromApi(placeId)
            .andThen(Completable.defer { removePlaceFromDb(placeId) })
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Completable.error(it) }
    }

    private fun removePlaceFromApi(placeId: Int): Completable {
        return visitedPlacesService.deletePlace(placeId)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Completable.error(it) }
            .doOnComplete {
                Log.d("PlacesRepository", "Place $placeId has been removed from API")
            }
    }

    private fun removePlaceFromDb(placeId: Int): Completable {
        return Completable.fromCallable { placesDao.deletePlace(placeId) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnComplete {
                Log.d("PlacesRepository", "Place $placeId has been removed from DB")
            }
    }

}