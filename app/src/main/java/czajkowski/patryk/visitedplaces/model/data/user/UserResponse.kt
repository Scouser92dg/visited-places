package czajkowski.patryk.visitedplaces.model.data.user

class UserResponse (
    val data: List<UserRemote>
)