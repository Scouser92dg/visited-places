package czajkowski.patryk.visitedplaces.model.repository

import org.koin.dsl.module

val repositoryModule = module {
    single { PlacesRepository(get(), get(), get(), get()) }
    single { UserRepository(get(), get()) }
    single { RatesRepository(get(), get()) }
}