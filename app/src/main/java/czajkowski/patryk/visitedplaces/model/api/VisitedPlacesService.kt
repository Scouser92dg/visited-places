package czajkowski.patryk.visitedplaces.model.api

import czajkowski.patryk.visitedplaces.model.data.place.EditPlaceRequest
import czajkowski.patryk.visitedplaces.model.data.place.NewPlaceRequest
import czajkowski.patryk.visitedplaces.model.data.place.PlaceRemote
import czajkowski.patryk.visitedplaces.model.data.place.PlaceResponse
import czajkowski.patryk.visitedplaces.model.data.rate.NewRateRequest
import czajkowski.patryk.visitedplaces.model.data.rate.RateResponse
import czajkowski.patryk.visitedplaces.model.data.user.NewUserRequest
import czajkowski.patryk.visitedplaces.model.data.user.UserResponse
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*

interface VisitedPlacesService {

    // places
    @GET("places")
    fun getPlaces(): Single<PlaceResponse>

    @GET("places/{id}")
    fun getPlace(@Path("id") id: Int): Single<PlaceRemote>

    @POST("places")
    fun addPlace(@Body request: NewPlaceRequest): Single<PlaceRemote>

    @DELETE("places/{id}")
    fun deletePlace(@Path("id") id: Int): Completable

    @PATCH("places/{id}")
    fun editPlace(@Path("id") id: Int, @Body request: EditPlaceRequest): Completable

    // rates
    @GET("rates")
    fun getRates(): Single<RateResponse>

    @POST("rates")
    fun addRate(@Body request: NewRateRequest): Completable

    //users
    @GET("users")
    fun getUsers(): Single<UserResponse>

    @POST("users")
    fun addUser(@Body request: NewUserRequest): Completable

}