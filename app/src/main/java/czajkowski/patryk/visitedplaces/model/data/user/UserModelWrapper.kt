package czajkowski.patryk.visitedplaces.model.data.user

import czajkowski.patryk.visitedplaces.model.data.user.UserLocal
import czajkowski.patryk.visitedplaces.model.data.user.UserRemote

data class UserModelWrapper (
    val id: String,
    val name: String?,
    val imageUrl: String?,
    var isLogged: Boolean = false
) {
    constructor(user: UserLocal): this(user.userId, user.name, user.imageUrl, user.isLogged)
    constructor(user: UserRemote): this(user.userId, user.name, user.imageUrl, false)
}