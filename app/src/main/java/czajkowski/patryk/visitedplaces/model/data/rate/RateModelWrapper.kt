package czajkowski.patryk.visitedplaces.model.data.rate

import czajkowski.patryk.visitedplaces.model.data.rate.RateLocal
import czajkowski.patryk.visitedplaces.model.data.rate.RateRemote

data class RateModelWrapper (
    var placeId: Int,
    var userId: String,
    var userImage: String?,
    var rate: Double?,
    var comment: String
) {
    constructor(rate: RateLocal): this(rate.placeId, rate.userId, rate.userImage, rate.rate, rate.comment)
    constructor(rate: RateRemote): this(rate.placeId, rate.userId, null, rate.rate, rate.comment?:"")
}