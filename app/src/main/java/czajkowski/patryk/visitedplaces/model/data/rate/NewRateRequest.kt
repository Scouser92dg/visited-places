package czajkowski.patryk.visitedplaces.model.data.rate

class NewRateRequest (
    var userId: String,
    var placeId: Int,
    var rate: Double = 0.0,
    var comment: String? = null
)