package czajkowski.patryk.visitedplaces.model.data.place

class PlaceResponse (
    val data: List<PlaceRemote>
)