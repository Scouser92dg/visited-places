package czajkowski.patryk.visitedplaces.model.data.rate

data class RateRemote (
    var placeId: Int,
    var userId: String,
    var comment: String?,
    var rate: Double?
)