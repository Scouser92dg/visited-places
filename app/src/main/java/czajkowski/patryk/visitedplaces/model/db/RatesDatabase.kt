package czajkowski.patryk.visitedplaces.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import czajkowski.patryk.visitedplaces.model.data.rate.RateLocal

@Database(entities = [RateLocal::class], version = RatesDatabase.VERSION)
abstract class RatesDatabase : RoomDatabase() {
    companion object {
        const val DB_NAME = "rates.db"
        const val VERSION = 4

        @Volatile
        private var INSTANCE: RatesDatabase? = null

        fun getInstance(context: Context): RatesDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: bindDatabase(context).also { INSTANCE = it }
            }

        private fun bindDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, RatesDatabase::class.java, DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }

    abstract fun rateDao(): RatesDao
}