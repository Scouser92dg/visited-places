package czajkowski.patryk.visitedplaces.model.repository

import android.util.Log
import czajkowski.patryk.visitedplaces.model.api.VisitedPlacesService
import czajkowski.patryk.visitedplaces.model.db.UserDao
import czajkowski.patryk.visitedplaces.model.data.user.NewUserRequest
import czajkowski.patryk.visitedplaces.model.data.user.UserLocal
import czajkowski.patryk.visitedplaces.model.data.user.UserModelWrapper
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers


class UserRepository(private val userDao: UserDao, private val service: VisitedPlacesService) {

    fun getUserFromApi(id: String): Observable<UserModelWrapper> {
        return service.getUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .map { it.data }
            .map { it.first { it.userId == id } }
            .map { UserModelWrapper(it) }
            .onErrorResumeNext { Single.error(it) }
            .toObservable()
            .doOnNext {
                Log.d("UserRepository", "Get User ${it.id} from Api")
            }
    }

    fun storeUser(user: UserModelWrapper): Completable {
        return storeUserOnApi(user)
            .andThen(Completable.defer { updateUserOnDb(user) })
    }

    fun updateUserOnDb(user: UserModelWrapper): Completable {
        return userDao.deleteUser(user.id)
            .andThen(Completable.defer {
                userDao.insertUser(
                    UserLocal(
                        name = user.name,
                        imageUrl = user.imageUrl,
                        isLogged = user.isLogged,
                        userId = user.id
                    )
                )
            })
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Completable.error(it) }
            .doOnComplete {
                Log.d("UserRepository", "Inserted User ${user.id} in DB...")
            }
    }

    private fun storeUserOnApi(user: UserModelWrapper): Completable {
        return service.getUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .flatMapCompletable {
                val userExisted = it.data.filter {
                    it.userId == user.id
                }.firstOrNull()
                if (userExisted == null) {
                    service.addUser(NewUserRequest(user.id, user.name, user.imageUrl))
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.io())
                        .doOnComplete {
                            Log.d("UserRepository", "Added User ${user.id} on API...")
                        }
                } else {
                    Completable.complete()
                }
            }
    }

    fun getLoggedUserFromDb(): Observable<UserModelWrapper> {
        return userDao.selectAllUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .toObservable()
            .map { it.filter { it.isLogged }.firstOrNull() }
            .map { UserModelWrapper(it) }
            .doOnNext {
                Log.d("UserRepository", "Dispatching User ${it.id} from DB...")
            }
    }

}