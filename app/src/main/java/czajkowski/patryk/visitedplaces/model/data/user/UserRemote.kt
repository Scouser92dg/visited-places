package czajkowski.patryk.visitedplaces.model.data.user

data class UserRemote (
    val userId: String,
    val name: String?,
    val imageUrl: String?
)