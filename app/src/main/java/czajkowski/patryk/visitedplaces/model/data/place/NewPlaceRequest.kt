package czajkowski.patryk.visitedplaces.model.data.place

class NewPlaceRequest (
    val latitude: Double,
    val longitude: Double,
    val country: String?,
    val city: String?,
    val visitedDate: String?,
    val description: String?,
    val userId: String
)