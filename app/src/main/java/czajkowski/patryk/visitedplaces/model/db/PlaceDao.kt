package czajkowski.patryk.visitedplaces.model.db

import androidx.room.*
import czajkowski.patryk.visitedplaces.model.data.place.PlaceLocal
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface PlaceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPlace(place: PlaceLocal)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllPlaces(places: List<PlaceLocal>)

    @Query("DELETE FROM PlaceLocal WHERE id = :placeId")
    fun deletePlace(placeId: Int)

    @Query("DELETE FROM PlaceLocal")
    fun deleteAllPlaces()

    @Query("DELETE FROM PlaceLocal WHERE userId = :userId")
    fun deletaUserPlaces(userId: String)

    @Query("SELECT * from PlaceLocal where id = :placeId")
    fun selectPlaceById(placeId: Int): Maybe<PlaceLocal>

    @Query("SELECT * from PlaceLocal where latitude = :latitude AND longitude = :longitude AND userId = :userId LIMIT 1")
    fun selectPlaceByLocation(userId: String, latitude: Double, longitude: Double): Single<PlaceLocal>

    @Query("SELECT * from PlaceLocal")
    fun selectAllPlaces(): Single<List<PlaceLocal>>

    @Query("SELECT * from PlaceLocal where userId = :userId")
    fun selectAllPlaces(userId: String): Single<List<PlaceLocal>>

    @Query("SELECT * from PlaceLocal where country LIKE '%' || :textFilter || '%' OR city LIKE '%' || :textFilter || '%' OR description LIKE '%' || :textFilter || '%'")
    fun searchPlacesByFilter(textFilter: String): Single<List<PlaceLocal>>

}