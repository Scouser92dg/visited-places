package czajkowski.patryk.visitedplaces.model.db

import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val daoModule = module {

    single<PlaceDao> { PlaceDao_Impl(PlacesDatabase.getInstance(androidApplication())) }
    single<UserDao> { UserDao_Impl(UserDatabase.getInstance(androidApplication())) }
    single<RatesDao> { RatesDao_Impl(RatesDatabase.getInstance(androidApplication())) }

}