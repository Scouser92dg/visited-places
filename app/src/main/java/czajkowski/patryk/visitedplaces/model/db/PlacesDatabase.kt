package czajkowski.patryk.visitedplaces.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import czajkowski.patryk.visitedplaces.model.data.place.PlaceLocal

@Database(entities = [PlaceLocal::class], version = PlacesDatabase.VERSION)
abstract class PlacesDatabase : RoomDatabase() {
    companion object {
        const val DB_NAME = "places.db"
        const val VERSION = 11

        @Volatile
        private var INSTANCE: PlacesDatabase? = null

        fun getInstance(context: Context): PlacesDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: bindDatabase(context).also { INSTANCE = it }
            }

        private fun bindDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, PlacesDatabase::class.java, DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }

    abstract fun placeDao(): PlaceDao
}