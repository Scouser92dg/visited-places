package czajkowski.patryk.visitedplaces.model.repository

import android.accounts.NetworkErrorException
import android.provider.Telephony
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import czajkowski.patryk.visitedplaces.BaseTest
import czajkowski.patryk.visitedplaces.model.api.StubVisitedPlacesService
import czajkowski.patryk.visitedplaces.model.api.VisitedPlacesService
import czajkowski.patryk.visitedplaces.model.data.place.*
import czajkowski.patryk.visitedplaces.model.data.rate.*
import czajkowski.patryk.visitedplaces.model.db.RatesDao
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import java.io.IOException

class RatesRepositoryTest  : BaseTest() {
    lateinit var repository: RatesRepository

    private val dao: RatesDao = mock()
    private val service: VisitedPlacesService = mock()

    private val ratesLocal: List<RateLocal> = listOf(
        RateLocal(1,1,"1","image.jpg",4.0,"fajne miejsce"),
        RateLocal(2,1,"2","image.jpg",3.0,"średnie miejsce miejsce")
    )

    private val ratesRemote: List<RateRemote> = listOf(
        RateRemote(1,"1","fajne miejsce",4.0),
        RateRemote(1,"2","średnie miejsce miejsce",3.0)
    )

    private val ratesModelWrapper: List<RateModelWrapper> = listOf(
        RateModelWrapper(ratesLocal.get(0)),
        RateModelWrapper(ratesLocal.get(1))
    )

    @Before
    fun init() {
        repository = RatesRepository(service, dao)
    }

    @Test
    fun `should get rates when api and dao contains some rates`() {
        whenever(service.getRates()).thenReturn(Single.just(RateResponse(ratesRemote)))
        whenever(dao.selectRatesByPlaceId(any())).thenReturn(Single.just(ratesLocal))

        repository.getRates(1)
            .test()
            .assertNoErrors()
            .assertValueAt(0, ratesModelWrapper)

        verify(service).getRates()
        verify(dao).selectRatesByPlaceId(any())
    }

    @Test
    fun `should get rates when api contains some places and dao not`() {
        whenever(service.getRates()).thenReturn(StubVisitedPlacesService().getRates())
        whenever(dao.selectRatesByPlaceId(any())).thenReturn(Single.just(listOf()))

        repository.getRates(1)
            .test()
            .assertNoErrors()
            .assertValue {
                it == StubVisitedPlacesService().getRates().blockingGet().data
            }

        verify(service).getRates()
        verify(dao).selectRatesByPlaceId(any())

    }

    @Test
    fun `should not get rates and return error when api throw error`() {
        whenever(dao.selectRatesByPlaceId(any())).thenReturn(Single.just(ratesLocal))
        whenever(service.getRates()).thenReturn(Single.error(NetworkErrorException("500")))

        repository.getRates(1)
            .test()
            .assertError(NetworkErrorException::class.java)
    }

    @Test
    fun `should not get rates and return error when dao throw error`() {
        whenever(service.getRates()).thenReturn(Single.just(RateResponse(ratesRemote)))
        whenever(dao.selectRatesByPlaceId(any())).thenReturn(Single.error(IOException()))

        repository.getRates(1)
            .test()
            .assertError(IOException::class.java)
    }

    @Test
    fun `should add rate when api works fine`() {
        whenever(service.addRate(any())).thenReturn(Completable.complete())
        whenever(service.getRates()).thenReturn(Single.just(RateResponse(ratesRemote)))

        repository.addRate(NewRateRequest("3",1,2.0, "no średnio"))
            .test()
            .assertComplete()

        verify(service).addRate(any())
    }

    @Test
    fun `should not add rate and return error when api throw error`() {
        whenever(service.getRates()).thenReturn(Single.just(RateResponse(ratesRemote)))
        whenever(service.addRate(any())).thenReturn(Completable.error(NetworkErrorException("500")))

        repository.addRate(NewRateRequest("4", 1, 2.0, "no średnio")).test()
            .assertError(NetworkErrorException::class.java)
    }

}