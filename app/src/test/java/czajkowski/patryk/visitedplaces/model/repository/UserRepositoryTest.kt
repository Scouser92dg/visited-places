package czajkowski.patryk.visitedplaces.model.repository

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import czajkowski.patryk.visitedplaces.BaseTest
import czajkowski.patryk.visitedplaces.model.api.VisitedPlacesService
import czajkowski.patryk.visitedplaces.model.db.UserDao
import czajkowski.patryk.visitedplaces.model.data.user.UserLocal
import czajkowski.patryk.visitedplaces.model.data.user.UserModelWrapper
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import java.io.IOException

class UserRepositoryTest  : BaseTest() {
    lateinit var repository: UserRepository

    private val dao: UserDao = mock()
    private val service: VisitedPlacesService = mock()
    private val user = UserLocal(id = 1, name = "Patryk Czajkowski", imageUrl = "profileImage.png", userId = "1", isLogged = true)
    private val userModelWrapper: UserModelWrapper = UserModelWrapper(user)

    @Before
    fun init() {
        repository = UserRepository(dao, service)
    }

    @Test
    fun `should get user`() {
        whenever(dao.selectAllUsers()).thenReturn(Single.just(listOf(user)))
        whenever(dao.selectUser(userId = "1")).thenReturn(Single.just(user))

        repository.getLoggedUserFromDb()
            .test()
            .assertNoErrors()
            .assertValue(userModelWrapper)
            .assertComplete()

    }

    @Test
    fun `should not get user when dao return error`() {
        whenever(dao.selectAllUsers()).thenReturn(Single.error(IOException()))

        repository.getLoggedUserFromDb()
            .test()
            .assertError(IOException::class.java)
            .assertNotComplete()
    }

    @Test
    fun `should save user`() {
        whenever(dao.insertUser(any())).thenReturn(Completable.complete())
        whenever(dao.deleteUser(any())).thenReturn(Completable.complete())

        repository.updateUserOnDb(userModelWrapper)
            .test()
            .assertNoErrors()
            .assertComplete()

        verify(dao).deleteUser(any())
        verify(dao).insertUser(any())
    }

}