package czajkowski.patryk.visitedplaces.model.repository

import android.accounts.NetworkErrorException
import com.nhaarman.mockitokotlin2.*
import czajkowski.patryk.visitedplaces.BaseTest
import czajkowski.patryk.visitedplaces.model.api.StubVisitedPlacesService
import czajkowski.patryk.visitedplaces.model.api.VisitedPlacesService
import czajkowski.patryk.visitedplaces.model.data.place.*
import czajkowski.patryk.visitedplaces.model.data.rate.RateModelWrapper
import czajkowski.patryk.visitedplaces.model.data.user.UserModelWrapper
import czajkowski.patryk.visitedplaces.model.db.PlaceDao
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import java.io.IOException

class PlacesRepositoryTest  : BaseTest() {
    lateinit var repository: PlacesRepository

    private val dao: PlaceDao = mock()
    private val userRepository: UserRepository = mock()
    private val ratesRepository: RatesRepository = mock()
    private val service: VisitedPlacesService = mock()
    private val addPlaceRequest = NewPlaceRequest(
        latitude = 50.50,
        longitude = 30.0,
        country = "Atlantyda",
        city = "Antalntyda Town",
        visitedDate = "1992-02-02",
        description = "I was there when I was young",
        userId = "123123"
    )

    private val placesLocal = listOf(
        PlaceLocal(1, "1", 50.0,50.0, "Poland", "Dąbrowa Górnicza", "Pogoria 4", "2020-06-06"),
        PlaceLocal(2, "1",40.0, 40.0, "France", "Paris", "Eiffel Tower", "2020-06-06")
    )

    private val placesRemote = listOf(
        PlaceRemote(1, "1", 50.0,50.0, "Poland", "Dąbrowa Górnicza", "Pogoria 4", "2020-06-06"),
        PlaceRemote(2, "1",40.0, 40.0, "France", "Paris", "Eiffel Tower", "2020-06-06")
    )

    private val placesModelWrapper = listOf(
        PlaceModelWrapper(1,"1",50.0, 50.0, "Poland", "Dąbrowa Górnicza", "Pogoria 4", "2020-06-06", rate = 2.0),
        PlaceModelWrapper(2, "1",40.0, 40.0, "France", "Paris", "Eiffel Tower", "2020-06-06", rate = 2.0)
    )

    private val userModelWrapper = UserModelWrapper("1","Patryk","image.jpg",true)

    @Before
    fun init() {
        repository = PlacesRepository(service, userRepository, ratesRepository, dao)
    }

    @Test
    fun `should get places when api and dao contains some places`() {
        whenever(service.getPlaces()).thenReturn(Single.just(PlaceResponse(placesRemote)))
        whenever(userRepository.getLoggedUserFromDb()).thenReturn(Observable.just(userModelWrapper))
        whenever(ratesRepository.getRates(any())).thenReturn(Observable.just(listOf(RateModelWrapper(1,"1","image.jpg", 2.0, "s"))))
        whenever(dao.selectAllPlaces(any())).thenReturn(Single.just(placesLocal))

        repository.getUserPlaces()
            .test()
            .assertNoErrors()
            .assertValueAt(0, placesModelWrapper)

        verify(service).getPlaces()
        verify(dao).selectAllPlaces("1")
    }

    @Test
    fun `should get places when api contains some places and dao not`() {
        whenever(service.getPlaces()).thenReturn(Single.just(PlaceResponse(placesRemote)))
        whenever(userRepository.getLoggedUserFromDb()).thenReturn(Observable.just(userModelWrapper))
        whenever(ratesRepository.getRates(any())).thenReturn(Observable.just(listOf(RateModelWrapper(1,"1","image.jpg", 2.0, "s"))))
        whenever(dao.selectAllPlaces(any())).thenReturn(Single.just(listOf()))

        repository.getUserPlaces()
            .test()
            .assertNoErrors()
            .assertValue {
                it == placesModelWrapper
            }

                verify(service).getPlaces()
                verify(dao).selectAllPlaces("1")

    }

    @Test
    fun `should not get places and return error places when dao throw error`() {
        whenever(service.getPlaces()).thenReturn(Single.just(PlaceResponse(listOf())))
        whenever(userRepository.getLoggedUserFromDb()).thenReturn(Observable.just(userModelWrapper))
        whenever(dao.selectAllPlaces(any())).thenReturn(Single.error(IOException()))

        repository.getUserPlaces()
            .test()
            .assertError(IOException::class.java)
    }

    @Test
    fun `should addPlace when api works fine`() {
        whenever(service.addPlace(any())).thenReturn(Single.just(placesRemote.get(0)))

        repository.addPlace(addPlaceRequest)
            .test()
            .assertComplete()

        verify(service).addPlace(any())
    }

    @Test
    fun `should not add place and return error when api throw error`() {
        whenever(service.addPlace(any())).thenReturn(Single.error(NetworkErrorException("500")))

        repository.addPlace(addPlaceRequest).test()
            .assertError(NetworkErrorException::class.java)
            .assertNotComplete()
    }

    @Test
    fun `should remove place from api`() {
        whenever(service.deletePlace(any())).thenReturn(Completable.complete())

        repository.removePlace(1)
            .test()
            .assertComplete()

        verify(service).deletePlace(any())
        verify(dao).deletePlace(any())
    }

}