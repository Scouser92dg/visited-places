package czajkowski.patryk.visitedplaces.viewmodel.home.map

import android.accounts.NetworkErrorException
import com.nhaarman.mockitokotlin2.*
import czajkowski.patryk.visitedplaces.viewmodel.BaseViewModelTest
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import java.io.IOException

class MapViewModelTest : BaseViewModelTest() {

    lateinit var mapViewModel: MapViewModel

    @Before
    override fun init() {
        super.init()

        mapViewModel = MapViewModel(userRepository, placesRepository, ratesRepository)
    }

    @Test
    fun `should load places on init`() {
        mapViewModel.places.value?.let {
            assert(it.containsAll(places.map { it }))
        }
    }

    @Test
    fun `should fetch places to refresh list after removed some place`() {
        mapViewModel.removePlace(1)

        verify(placesRepository, atLeast(1)).getUserPlaces()
    }

    @Test
    fun `should show error message when some problems during fetch places`() {
        whenever(placesRepository.getUserPlaces()).thenReturn(
            Observable.error(
                NetworkErrorException()
            )
        )

        mapViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }

    @Test
    fun `should show error message and logout when some problems during get user`() {
        whenever(userRepository.getLoggedUserFromDb()).thenReturn(Observable.error(IOException()))

        mapViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }

    @Test
    fun `should logout when problem with user load`() {
        whenever(userRepository.getLoggedUserFromDb()).thenReturn(Observable.error(IOException()))

        mapViewModel.isLogout.value?.let {
            assert(true)
        }
    }

}