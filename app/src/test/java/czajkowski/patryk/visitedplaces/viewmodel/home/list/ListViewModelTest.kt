package czajkowski.patryk.visitedplaces.viewmodel.home.list

import android.accounts.NetworkErrorException
import com.nhaarman.mockitokotlin2.*
import czajkowski.patryk.visitedplaces.viewmodel.BaseViewModelTest
import io.reactivex.Completable
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import java.io.IOException

class ListViewModelTest : BaseViewModelTest() {

    lateinit var listViewModel: ListViewModel

    @Before
    override fun init() {
        super.init()

        listViewModel = ListViewModel(placesRepository)
    }


    @Test
    fun `should load places on init`() {
        listViewModel.places.value?.let { list ->
            assert(list.containsAll(places.map { it }))
        }
    }

    @Test
    fun `should fetch places to refresh list after removed some place`() {
        listViewModel.removePlace(1)

        verify(placesRepository, atLeast(1)).getUserPlaces()
    }

    @Test
    fun `should show error message when some problems during fetch places`() {
        whenever(placesRepository.getUserPlaces()).thenReturn(Observable.error(NetworkErrorException()))

        listViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }

    @Test
    fun `should show error message when some problems during delete place`() {
        whenever(placesRepository.removePlace(1)).thenReturn(Completable.error(IOException()))

        listViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }

    @Test
    fun `should show error message and logout when some problems during get user`() {
        whenever(userRepository.getLoggedUserFromDb()).thenReturn(Observable.error(IOException()))

        listViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }

    @Test
    fun `should logout when problem with user load`() {
        whenever(userRepository.getLoggedUserFromDb()).thenReturn(Observable.error(IOException()))

        listViewModel.isLogout.value?.let {
            assert(true)
        }
    }

}