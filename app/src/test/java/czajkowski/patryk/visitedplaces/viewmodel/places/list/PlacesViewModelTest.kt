package czajkowski.patryk.visitedplaces.viewmodel.places.list

import android.accounts.NetworkErrorException
import com.nhaarman.mockitokotlin2.whenever
import czajkowski.patryk.visitedplaces.viewmodel.BaseViewModelTest
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import java.io.IOException

class PlacesViewModelTest : BaseViewModelTest() {

    lateinit var placesViewModel: PlacesViewModel

    @Before
    override fun init() {
        super.init()
        whenever(placesRepository.getOtherUserPlaces()).thenReturn(Observable.just(places))

        placesViewModel = PlacesViewModel(placesRepository)
    }

    @Test
    fun `should load places on init`() {

        placesViewModel.places.value?.let { list ->
            assert(list.containsAll(places.map { it }))
        }
    }

    @Test
    fun `should show error message when some problems during fetch places`() {
        whenever(placesRepository.getOtherUserPlaces()).thenReturn(Observable.error(NetworkErrorException()))

        placesViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }


    @Test
    fun `should show error message when some problems during get user`() {
        whenever(userRepository.getLoggedUserFromDb()).thenReturn(Observable.error(IOException()))

        placesViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }

}