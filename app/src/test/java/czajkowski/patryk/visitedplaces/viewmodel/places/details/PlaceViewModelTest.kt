package czajkowski.patryk.visitedplaces.viewmodel.places.details

import android.accounts.NetworkErrorException
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import czajkowski.patryk.visitedplaces.viewmodel.BaseViewModelTest
import io.reactivex.Completable
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test

class PlaceViewModelTest : BaseViewModelTest() {

    lateinit var placeViewModel: PlaceViewModel

    @Before
    override fun init() {
        super.init()
        whenever(placesRepository.getPlace(any())).thenReturn(Observable.just(places.get(0)))

        placeViewModel = PlaceViewModel(placesRepository, ratesRepository, userRepository)
    }

    @Test
    fun `should load places on init`() {

        placeViewModel.place.value?.let {
            assert(it.equals(places.get(0)))
        }
    }

    @Test
    fun `should show error message when some problems during fetch place`() {
        whenever(placesRepository.getPlace(any())).thenReturn(Observable.error(
            NetworkErrorException()
        ))

        placeViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }

    @Test
    fun `should button save disabled after rate`() {
        whenever(userRepository.getLoggedUserFromDb()).thenReturn(Observable.just(user))
        whenever(ratesRepository.addRate(any())).thenReturn(Completable.complete())

        placeViewModel.ratePlace(2.0F,"")

        placeViewModel.isButtonEnabled.equals(false)
    }


    @Test
    fun `should go to list after rate`() {
        whenever(userRepository.getLoggedUserFromDb()).thenReturn(Observable.just(user))
        whenever(ratesRepository.addRate(any())).thenReturn(Completable.complete())

        placeViewModel.ratePlace(2.0F,"")

        placeViewModel.isGoToPlaces.equals(false)
    }

}