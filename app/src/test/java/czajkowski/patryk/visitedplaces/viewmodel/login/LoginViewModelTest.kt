package czajkowski.patryk.visitedplaces.viewmodel.login

import com.facebook.AccessToken
import com.nhaarman.mockitokotlin2.mock
import czajkowski.patryk.visitedplaces.viewmodel.BaseViewModelTest
import org.junit.Before
import org.junit.Test

class LoginViewModelTest  : BaseViewModelTest() {

    lateinit var loginViewModel: LoginViewModel

    private val accessToken: AccessToken = mock()

    @Before
    override fun init() {
        super.init()

        loginViewModel = LoginViewModel(userRepository)
    }

    @Test
    fun `should go to home after login`() {
        loginViewModel.loginByFacebook(accessToken)

        loginViewModel.errorMessage.value?.let {
            assert(it.isBlank())
        }

        loginViewModel.isGoToHome.value?.let {
            assert(it)
        }
    }

}