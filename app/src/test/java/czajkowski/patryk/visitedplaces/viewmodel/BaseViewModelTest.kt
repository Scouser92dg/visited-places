package czajkowski.patryk.visitedplaces.viewmodel

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import czajkowski.patryk.visitedplaces.BaseTest
import czajkowski.patryk.visitedplaces.model.data.place.PlaceModelWrapper
import czajkowski.patryk.visitedplaces.model.data.user.UserModelWrapper
import czajkowski.patryk.visitedplaces.model.repository.PlacesRepository
import czajkowski.patryk.visitedplaces.model.repository.RatesRepository
import czajkowski.patryk.visitedplaces.model.repository.UserRepository
import io.reactivex.Completable
import io.reactivex.Observable
import org.junit.Before

open class BaseViewModelTest: BaseTest() {
    protected open val placesRepository: PlacesRepository = mock()
    protected open val userRepository: UserRepository = mock()
    protected open val ratesRepository: RatesRepository = mock()

    protected open val places = listOf(
        PlaceModelWrapper(1, "1", 50.0,50.0, "Poland", "Dąbrowa Górnicza", "Pogoria 4", "", "", 5.0),
        PlaceModelWrapper(2, "1",40.0, 40.0, "France", "Paris", "Eiffel Tower", "", "", 5.0)
    )

    protected open val user = UserModelWrapper(id = "1", name = "Patryk Czajkowski", imageUrl = "profileImage.png")

    @Before
    open fun init() {
        whenever(userRepository.getLoggedUserFromDb()).thenReturn(Observable.just(user))
        whenever(placesRepository.getUserPlaces()).thenReturn(Observable.just(places))
        whenever(placesRepository.removePlace(any())).thenReturn(Completable.complete())
    }

}