package czajkowski.patryk.visitedplaces.viewmodel.profile

import czajkowski.patryk.visitedplaces.viewmodel.BaseViewModelTest
import org.junit.Before
import org.junit.Test

class ProfileViewModelTest : BaseViewModelTest() {

    lateinit var profileViewModel: ProfileViewModel

    @Before
    override fun init() {
        super.init()

        profileViewModel = ProfileViewModel(userRepository, placesRepository)
    }

    @Test
    fun `should load places on init`() {
        profileViewModel.places.value?.let {
            assert(it.containsAll(places.map { it }))
        }
        profileViewModel.errorMessage.value?.let {
            assert(it.isEmpty())
        }
        profileViewModel.isLogout.value?.let {
            assert(false)
        }
    }

    @Test
    fun `should load user on init`() {
        profileViewModel.user.value?.let {
            assert(it == user)
        }
        profileViewModel.errorMessage.value?.let {
            assert(it.isEmpty())
        }
        profileViewModel.isLogout.value?.let {
            assert(false)
        }
    }

    @Test
    fun `should show error message on load user failed`() {
        profileViewModel.user.value?.let {
            assert(it != user)
        }
        profileViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }

    @Test
    fun `should show error message on load places failed`() {
        profileViewModel.user.value?.let {
            assert(it == user)
        }
        profileViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }

    @Test
    fun `should logout when problem with user load`(){
        profileViewModel.isLogout.value?.let {
            assert(true)
        }
    }

}