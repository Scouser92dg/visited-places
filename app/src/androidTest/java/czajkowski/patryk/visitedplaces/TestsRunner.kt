package czajkowski.patryk.visitedplaces

import czajkowski.patryk.visitedplaces.viewmodel.home.list.ListFragmentTest
import czajkowski.patryk.visitedplaces.viewmodel.home.map.MapFragmentTest
import czajkowski.patryk.visitedplaces.viewmodel.login.LoginFragmentTest
import czajkowski.patryk.visitedplaces.viewmodel.profile.ProfileFragmentTest
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.junit.runners.Suite.SuiteClasses


@RunWith(Suite::class)
@SuiteClasses(
    LoginFragmentTest::class,
    MapFragmentTest::class,
    ListFragmentTest::class,
    ProfileFragmentTest::class
)
class TestsRunner {
}