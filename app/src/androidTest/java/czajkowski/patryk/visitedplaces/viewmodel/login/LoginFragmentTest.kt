package czajkowski.patryk.visitedplaces.viewmodel.login

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiScrollable
import androidx.test.uiautomator.UiSelector
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import czajkowski.patryk.visitedplaces.viewmodel.BaseFragmentTest
import czajkowski.patryk.visitedplaces.R
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters


@RunWith(AndroidJUnit4::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@LargeTest
class LoginFragmentTest : BaseFragmentTest() {

    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var googleSignInOptions: GoogleSignInOptions

    @Before
    fun init() {
        googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(InstrumentationRegistry.getInstrumentation().targetContext, googleSignInOptions)
        googleSignInClient.signOut()
        LoginManager.getInstance().logOut()

        launchFragment(R.id.loginFragment)
        waitUntilViewIsDisplayed(ViewMatchers.withId(R.id.loginFragmentLayout))
    }

    @Test
    fun checkIfFacebookLoginWorks() {
        Espresso.onView(ViewMatchers.withId(R.id.loginFacebookButton))
            .perform(ViewActions.click())

        waitUntilViewIsDisplayed(ViewMatchers.withId(R.id.homeFragmentLayout))

        Espresso.onView(ViewMatchers.withId(R.id.homeFragmentLayout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun checkIfGoogleLoginWorks() {
        Espresso.onView(ViewMatchers.withId(R.id.loginGoogleButton))
            .perform(ViewActions.click())

        val items = UiScrollable(UiSelector().className("android.support.v7.widget.RecyclerView"))

        items.setAsVerticalList()
        items.clickTopLeft()

        waitUntilViewIsDisplayed(ViewMatchers.withId(R.id.homeFragmentLayout))

        Espresso.onView(ViewMatchers.withId(R.id.homeFragmentLayout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

}