package czajkowski.patryk.visitedplaces.viewmodel.home.list

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import czajkowski.patryk.visitedplaces.viewmodel.BaseFragmentTest
import czajkowski.patryk.visitedplaces.R
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters


@RunWith(AndroidJUnit4::class)
@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class ListFragmentTest: BaseFragmentTest() {

    @Before
    fun init() {
        launchFragment(R.id.homeFragment)

        waitUntilViewIsDisplayed(withId(R.id.homeFragmentLayout))

        onView(withId(R.id.homeListMenuItem))
            .perform(ViewActions.click())

        waitUntilViewIsDisplayed(withId(R.id.listFragmentLayout))
    }

    @Test
    fun checkIfDialogAfterRemoveClickShown() {
        Thread.sleep(1000)

        onView(withId(R.id.placesRecycler))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, clickOnViewChild(R.id.placeItemRemoveIcon)))

        onView(withText(activityRule.activity.getString(android.R.string.cancel)))
            .check(matches(isDisplayed()))

        onView(withText(activityRule.activity.getString(android.R.string.ok)))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkIfSwitchToProfileFragmentOnMenuItemClick() {
        onView(withId(R.id.homeListProfileMenuItem))
            .perform(ViewActions.click())

        waitUntilViewIsDisplayed(withId(R.id.profileFragmentLayout))

        onView(withId(R.id.profileFragmentLayout))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkIfDialogCloseOnOKClick() {
        Thread.sleep(1000)

        onView(withId(R.id.placesRecycler))
            .perform(RecyclerViewActions
                .actionOnItemAtPosition<RecyclerView.ViewHolder>(0, clickOnViewChild(R.id.placeItemRemoveIcon)))

        onView(withText(activityRule.activity.getString(android.R.string.ok)))
            .perform(ViewActions.click())

        onView(withId(R.id.placesRecycler))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkIfDialogCloseOnCancelClick() {
        Thread.sleep(1000)

        onView(withId(R.id.placesRecycler))
            .perform(RecyclerViewActions
                .actionOnItemAtPosition<RecyclerView.ViewHolder>(0, clickOnViewChild(R.id.placeItemRemoveIcon)))

        onView(withText(activityRule.activity.getString(android.R.string.cancel)))
            .perform(ViewActions.click())

        onView(withId(R.id.placesRecycler))
            .check(matches(isDisplayed()))
    }

}