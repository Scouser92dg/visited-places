package czajkowski.patryk.visitedplaces.viewmodel.home.map

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.uiautomator.UiSelector
import czajkowski.patryk.visitedplaces.R
import czajkowski.patryk.visitedplaces.viewmodel.BaseFragmentTest
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters


@RunWith(AndroidJUnit4::class)
@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class MapFragmentTest: BaseFragmentTest() {

    companion object {
        const val MATERIAL_DIALOG_INPUT_ID = 2131296522
        const val PLACE_TEST_DESCRIPTION = "testdescription"
    }

    @Before
    fun init() {
        launchFragment(R.id.homeFragment)
        waitUntilViewIsDisplayed(withId(R.id.homeFragmentLayout))
    }

    @Test
    fun checkIfAddPlaceWorking() {
        addPlace()

        waitUntilViewIsDisplayed(withId(R.id.mapFragmentLayout))

        val isAdded = device.findObject(UiSelector().descriptionContains(PLACE_TEST_DESCRIPTION)) != null
        assert(isAdded)
    }

    @Test
    fun checkIfIconRemoveAppearsOnMarkerClicked() {
        device.findObject(UiSelector().descriptionContains(PLACE_TEST_DESCRIPTION)).click()

        Thread.sleep(1000)

        onView(withMenuIdOrText(R.id.homeMapRemoveMenuItem, R.string.home_remove_label)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun checkIfMarkerAndRemoveIconDisappearsOnRemoveClick() {
        device.findObject(UiSelector().descriptionContains(PLACE_TEST_DESCRIPTION)).click()

        Thread.sleep(1000)

        onView(withMenuIdOrText(R.id.homeMapRemoveMenuItem, R.string.home_remove_label))
            .perform(ViewActions.click())

        assert(device.findObject(UiSelector().descriptionContains(PLACE_TEST_DESCRIPTION)) == null)

        addPlace()
        addPlace()
    }

    private fun addPlace() {
        onView(withId(R.id.mapView))
            .perform(ViewActions.longClick())

        onView(ViewMatchers.withText(activityRule.activity.getString(android.R.string.ok)))
            .perform(ViewActions.click())

        onView(withId(MATERIAL_DIALOG_INPUT_ID))
            .perform(ViewActions.typeText(PLACE_TEST_DESCRIPTION))

        onView(ViewMatchers.withText(activityRule.activity.getString(android.R.string.ok)))
            .perform(ViewActions.click())
    }

    @Test
    fun checkIfSwitchToProfileFragmentOnMenuItemClick() {
        onView(withId(R.id.homeMapMenuItem))
            .perform(ViewActions.click())

        onView(withId(R.id.homeMapProfileMenuItem))
            .perform(ViewActions.click())

        onView(withId(R.id.profileFragmentLayout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

}