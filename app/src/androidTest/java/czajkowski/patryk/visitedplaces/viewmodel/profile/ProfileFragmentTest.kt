package czajkowski.patryk.visitedplaces.viewmodel.profile

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import czajkowski.patryk.visitedplaces.R
import czajkowski.patryk.visitedplaces.viewmodel.BaseFragmentTest
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters


@RunWith(AndroidJUnit4::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@LargeTest
class ProfileFragmentTest : BaseFragmentTest() {

    @Before
    fun init() {
        launchFragment(R.id.profileFragment)
        waitUntilViewIsDisplayed(withId(R.id.profileFragmentLayout))
    }

    @Test
    fun checkIfAllViewsFilled() {
        Thread.sleep(1000)

        onView(withId(R.id.profileNameLabel)).check(matches(not(withText(""))))
        onView(withId(R.id.profileCitiesVisitedText)).check(matches(not(withText(""))))
        onView(withId(R.id.profileCountriesVisitedText)).check(matches(not(withText(""))))
        onView(withId(R.id.profileMostPlacesYearText)).check(matches(not(withText(""))))
        onView(withId(R.id.profilePlacesThisYearText)).check(matches(not(withText(""))))
    }

    @Test
    fun checkIfLogoutWorks() {
        onView(withMenuIdOrText(R.id.profileLogoutMenuItem, R.string.profile_logout_label)).perform(click())

        onView(withId(R.id.loginFragmentLayout))
            .check(matches(isDisplayed()))
    }

}